export const navItems = [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'warning',
        text: 'NEW'
      }
    },
    {
      title: true,
      name: 'Danh mục hàng hóa'
    },
    {
      name: 'Hàng Hóa',
      url: '/hanghoa',
      icon: 'fa fa-product-hunt'
    },
    {
      title: true,
      name: 'Danh mục phiếu nhập hàng'
    },
    {
      name: 'Phiếu Nhập Hàng',
      url: '/phieunhap',
      icon: 'fa fa-share-square-o'
    },
    {
      title: true,
      name: 'Danh mục bàn'
    },
    {
        name: 'Phòng Bàn',
        url: '/phongban',
        icon: 'fa fa-table'
      },
      {
        title:true,
        name: 'Thống kê'
      },
      {
        name:'Tồn Kho',
        url:'/thongketonkho',
        icon:'fa fa-filter'
      },
      {
        name:'Doanh Thu',
        url:'/thongketonkho',
        icon:'fa fa-filter'
      },
      {
        name:'Nhập Hàng',
        url:'/thongkenhaphang',
        icon:'fa fa-filter'
      }
  
  ];