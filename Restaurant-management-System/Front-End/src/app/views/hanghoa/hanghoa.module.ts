import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridModule } from '../../../../node_modules/@progress/kendo-angular-grid';
import {FormsModule} from '@angular/forms';
import { HangHoaRoutingModule } from './hanghoa-routing.module';
import { HanghoaComponent } from './hanghoa.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { HanghoaDetailComponent } from './hanghoa-detail/hanghoa-detail.component';
import { HanghoaChinhsuaComponent } from './hanghoa-chinhsua/hanghoa-chinhsua.component';
import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';

@NgModule({
  imports: [
    CommonModule,
    HangHoaRoutingModule,
    GridModule,
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    AccordionModule.forRoot(),
    TypeaheadModule.forRoot(),
    FormsModule,
    LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.threeBounce,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)', 
      primaryColour: '#ff6358', 
      secondaryColour: '#ff6358', 
      tertiaryColour: '#ff6358'
  })

  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  declarations: [ HanghoaComponent, HanghoaDetailComponent, HanghoaChinhsuaComponent ],
  
})
export class HangHoaModule { }