import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { HangHoa } from '../../models/hanghoa.class';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { HangHoaService, MonThemService } from '../../shared/hoanghoa.service';
import { ToastrService } from 'ngx-toastr';
import {HanghoaComponent} from '../hanghoa.component';
@Component({
  selector: 'hanghoa-detail',
  templateUrl: './hanghoa-detail.component.html',
  styleUrls: ['./hanghoa-detail.component.scss']
})
export class HanghoaDetailComponent implements OnInit {
 

  @Input() dataItem: HangHoa;

  constructor(private hanghoacompo:HanghoaComponent,
    private service: HangHoaService, 
    private servicemonthem: MonThemService,
    private toastr: ToastrService,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.loadMonThem();
  }
  loadMonThem(): void {
    if(!this.dataItem.lamonthem){
      this.servicemonthem.getAllMonThem(this.dataItem.hanghoaID);
      console.log(this.servicemonthem.listmonthem.length);
    }
    else{
      this.servicemonthem.getAllMonDuocThem(this.dataItem.hanghoaID);
    }
    
  }
  isDelete: boolean;
    //lấy thông tin hanghoa được chọn
    getHanghoaSelected(dataItem){
      //this.service.titleSelected=false;
      this.service.hanghoaItemSelected = dataItem;
    }
   //modal confirm
   modalRef: BsModalRef;
   openModal(template: TemplateRef<any>) {
     this.modalRef = this.modalService.show(template, {class: 'gray modal-sm modal-dialog-centered'});
   }
   openModalUpdate(template: TemplateRef<any>) {
     this.modalRef = this.modalService.show(template, {class: 'gray modal-lg'});
   }
   //hàm xóa bàn và đổi trạng thái

   confirm(): void {
     if(this.isDelete){
      this.service.delete(this.service.hanghoaItemSelected.hanghoaID).subscribe((data: HangHoa)=> {
        this.toastr.error('Xóa thành công!','Xóa phòng/bàn');
        this.modalRef.hide();
        this.hanghoacompo.loadData();
      });
     }
     else{
      this.service.hanghoaItemSelected.trangthai = !this.service.hanghoaItemSelected.trangthai
       this.service.update(this.service.hanghoaItemSelected.hanghoaID,this.service.hanghoaItemSelected).subscribe(x=>{
        this.toastr.warning('Cập nhật trạng thái thành công!','Trạng thái của phòng/bàn');
        this.modalRef.hide();
        this.hanghoacompo.loadData();
       });
     }
   }
   decline(): void {
     this.modalRef.hide();
   }

}
