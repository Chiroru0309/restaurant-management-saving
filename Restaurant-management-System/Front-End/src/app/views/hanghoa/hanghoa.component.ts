import { Component, OnInit, ViewChild, AfterViewInit,TemplateRef } from '@angular/core';
import { HttpClient } from '../../../../node_modules/@angular/common/http';
import {NgForm} from '@angular/forms';
//import model ban
import { HangHoa } from '../models/hanghoa.class';
//import kendo UI
import { process, State } from '@progress/kendo-data-query';
import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
//import service
import {HangHoaService, ThucDonService} from '../shared/hoanghoa.service';

//import toastr
import { ToastrService } from 'ngx-toastr';

//import ngx-modal
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Response } from '@angular/http';
import {map} from 'rxjs/operators';
import { ThucDon } from '../models/thucdon.class';
import { NhomHang } from '../models/nhomhang.class';
import { LoaiHang } from '../models/loaihang.class';
@Component({
  selector: 'app-hanghoa',
  templateUrl: './hanghoa.component.html',
  styleUrls: ['./hanghoa.component.scss']
})
export class HanghoaComponent implements OnInit, AfterViewInit {
  public gridData: GridDataResult;
  public loading = false;
  public state: State = {
    skip: 0,
    take: 5,
  };
  public hanghoas: HangHoa[];
  public data: Object[];
  @ViewChild(GridComponent) grid: GridComponent;

  constructor(private service: HangHoaService, private toastr: ToastrService,private modalService: BsModalService ) { }

 
  ngOnInit() {
    this.resetItemSelected();
    this.loadData();
   
  }

  public ngAfterViewInit(): void {
    //mở rộng 1 hàng
    //this.grid.expandRow(0);
  }

  
  //thay đổi dữ liệu khi chuyển trang
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state=state;
    //reload dữ liệu
    this.loadData();
  }
  //load dữ liệu 
  loadData(): any {
    this.loading=true;
    this.service.getAllhanghoa()
    .pipe(map((data: Response)=>{
      return data.json() as HangHoa[];
    })).toPromise().then(x=>{
      this.hanghoas = x;
      this.hanghoas.reverse();
      this.service.listhanghoa= this.hanghoas;
          this.gridData =  process(this.hanghoas, this.state);
          this.loading=false;
    })    
  }
  //reset thông tin bàn đã được chọn
  resetItemSelected(){
    this.service.hanghoaItemSelected = {
      hanghoaID: null,
      mahanghoa: '',
      tenhang: '',
      giavon: 0,
      giaban: 0,
      tonkho: 0,
      dathang: 0,
      dmin: 0,
      dmnn: 0,
      mota:'',
      ghichu:'',
      lamonthem:false,
      hinhthucban:true,
      trangthai: true,
      thucDon: new ThucDon(),
      nhomHang: new NhomHang(),
      loaihang: new LoaiHang()
    };
  }

    modalRef: BsModalRef;
    openModal(template: TemplateRef<any>) {   
      this.modalRef = this.modalService.show(template, {class: 'gray modal-lg'});
    }
     //order 1 trang 
  pageList: number[] = [5,10,20,50,100];
  
  onPageSelectedChange(val: any){
    this.state.take = val;
    this.loadData();
   }
   states = [
    {name: 'Arizona', abbrev: 'AZ'},
    {name: 'California', abbrev: 'CA'},
    {name: 'Colorado', abbrev: 'CO'},
    {name: 'New York', abbrev: 'NY'},
    {name: 'Pennsylvania', abbrev: 'PA'},
  ];
  stateselected = this.states[1];
}
