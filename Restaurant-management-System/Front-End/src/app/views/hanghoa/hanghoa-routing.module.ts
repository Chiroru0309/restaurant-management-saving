import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HanghoaComponent } from './hanghoa.component';

const routes: Routes = [
  {
    path: '',
    component: HanghoaComponent,
    data: {
      title: 'HangHoa'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HangHoaRoutingModule {}
