import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { HangHoaService, ThucDonService, NhomHangService } from '../../shared/hoanghoa.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import{HanghoaComponent} from '../hanghoa.component';

@Component({
  selector: 'hanghoa-chinhsua',
  templateUrl: './hanghoa-chinhsua.component.html',
  styleUrls: ['./hanghoa-chinhsua.component.scss']
})
export class HanghoaChinhsuaComponent implements OnInit {

  @Input() isAdd:boolean;
  isThuocTinh:boolean = false;
  isDVT:boolean = false;
  isMonThem:boolean = false;
  constructor(private ref:ChangeDetectorRef, private service: HangHoaService,private toastr: ToastrService,
     private hanghoacomponent:HanghoaComponent,private thucdonservice:ThucDonService, private nhomhangservice:NhomHangService ) { }

  ngOnInit() {
    this.isMonThem = this.service.hanghoaItemSelected.lamonthem;
    this.thucdonservice.getAllThucDon();
    this.nhomhangservice.getAllNhomHang();
  }
  @Input() modalRef: BsModalRef;
  decline(): void {
    this.modalRef.hide();
  }
  
  //xử lý phần thuộc tính hàng hóa
  listThuoctinh:Object[] = [];
  themthuoctinh(){
    this.isThuocTinh=true;
    const thuoctinh:Object ={tenthuoctinh:'',giatri:''};
    this.listThuoctinh.push(thuoctinh);
  }

  removett(indextt:number){
    this.listThuoctinh.splice(indextt,1);
    if(this.listThuoctinh.length<= 0){
      this.isThuocTinh=false;
    }
  }

  //xử lý phần đơn vị tính hàng hóa
  listdvt:Object[] = [];
  themdvt(){
    this.isDVT=true;
    const dvt:Object ={tendonvi:'',giaquydoi:'0',giaban:'0'};
    this.listdvt.push(dvt);
  }

  removedvt(indextt:number){
    this.listdvt.splice(indextt,1);
    if(this.listdvt.length<= 0){
      this.isDVT=false;
    }
  }

  OnSubmit(hanghoaForm:NgForm){
    console.log(hanghoaForm.value);
    if(hanghoaForm.value.hanghoaID==null){
      delete hanghoaForm.value.hanghoaID;
      this.service.add(hanghoaForm.value).subscribe((data)=>
    {
      this.toastr.success("Thêm mới thành công!","Thêm mới phòng/bàn");
      this.hanghoacomponent.loadData();
    });
  }
  else{
    this.service.update(hanghoaForm.value.hanghoaID,hanghoaForm.value).subscribe((data)=>
    {
      this.toastr.info('Cập nhật thành công!','Cập nhật phòng/bàn');
      this.hanghoacomponent.loadData();
    });
  }
  }
}
