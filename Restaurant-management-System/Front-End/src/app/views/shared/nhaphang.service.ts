import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {NhapHang} from '../models/nhaphang.class';
import {ChiTietPhieuNhap} from '../models/chitietphieunhap.class';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class NhapHangService {

   API_URL: string = 'https://localhost:44368/api/chitietphieunhaps/getAllHangHoaByPhieuNhap';
  public nhaphangItemSelected: NhapHang = new NhapHang();

  constructor( public http: HttpClient) { }
  tongtien:number;
  getNhaphangById(id:number){
    return this.http.get(this.API_URL + id);
  }

  getTongtien(tien:number){
    this.tongtien = tien;
  }
  API_URL3 : string = 'https://localhost:44368/api/chitietphieunhaps'
  getAllnhaphang(){
      return this.http.get(this.API_URL3  );
  }

  getNhaphangtheoten(ten:string){
    
    return this.http.get(`${this.API_URL}/${ten}`);
  }
  API_URL2 =  'https://localhost:44368/api/chitietphieunhaps';
  add(mahanghoa: string,maphieunhap){
     return this.http.post(`${this.API_URL2}/${mahanghoa}/${maphieunhap}`,httpOptions);
  }

  
  update(nhaphang:ChiTietPhieuNhap){
     return this.http.put(`${this.API_URL3}/${nhaphang.chitietphieunhapID}`, nhaphang,httpOptions);
  }
  delete(id: number){
    return this.http.delete(`${this.API_URL + '/SP00001'}/${id}`);
  }
}
