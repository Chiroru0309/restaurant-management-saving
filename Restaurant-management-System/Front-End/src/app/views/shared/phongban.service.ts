import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders,HttpRequest} from '@angular/common/http';
import {Ban} from '../models/ban.class';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class PhongbanService {

   API_URL: string = 'https://localhost:44368/api/bans';
  public banItemSelected: Ban = {
    banID:null,
    tenban: '',
    nhombanID:-1,
    soghe:0,
    ghichu:'',
    trangthai: true
  };
  titleSelected: boolean = true;

  constructor( public http: HttpClient) { 
    
  }

 
  getAllPhongBan(){
   
      return this.http.get(this.API_URL);
  }
  getAllNhomBan(){
   
    return this.http.get(`${this.API_URL}/getnhombans`);
  }
  add(ban){
     return this.http.post(this.API_URL, ban,httpOptions);
  }
  update(ban: Ban){
     return this.http.put(`${this.API_URL}/${ban.banID}`, ban,httpOptions);
  }
  delete(id: number){
    return this.http.delete(`${this.API_URL}/${id}`);
  }
  getBanTheoNhom(id: number){
    return this.http.get(`${this.API_URL}/getbantheonhom/${id}`);
  }
}
