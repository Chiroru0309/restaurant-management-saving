import { Injectable } from '@angular/core';
import { Ban } from '../models/ban.class';
import { HangHoa } from '../models/hanghoa.class';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { HoaDon } from '../models/hoadonreq.class';
import { ChiTietHoaDonReq } from '../models/chitiethoadonreq.class';
import { ChiTietHoaDon } from '../models/ChiTietHoaDon.class';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
    providedIn: 'root'
  })
  export class BanCashService {
    ban:Ban=new Ban();
    tenmon:string;
    listTen=[]
    temp:any;
    test1:any;
    test2:any;
    holdtenban;
    
    getTenBan(ban:Ban){
      this.ban=ban;
    }

    tongtien=0
    soluong=0

    chitiethoadon:ChiTietHoaDonReq={
      chitiethoadonID:null,
      ghichu:null,
      giamgia:null,
      hanghoaID:1,
      hoadonID:1,
      soluong:1
    }
    getTenMon(mon:HangHoa){
        this.temp={"ten":this.ban.tenban,"mon":mon[0].tenhang,"soluong":1,"dongia":mon[0].giaban}
        this.test1 = { "ten": this.ban.tenban, "soluong": this.soluong++, "tongtien": this.tongtien += mon[0].giaban }
        // this.holdtenban=this.tenban;
        // this.test1={"ten":this.tenban,"data":[]}
        // console.log(this.test1)
        // if(this.holdtenban==this.tenban){
        // this.test2={"mon":mon[0].tenhang,"soluong":1,"dongia":mon[0].giaban}
        // }
        this.listTen.push(this.temp)

        
    }
    delOrder(i:number,b:any){
    console.log(b)
    console.log(this.test1.tongtien)
    console.log(b.dongia)
    console.log(this.test1.tongtien-=b.dongia)
      this.test1.soluong--
      this.test1.tongtien-b.dongia 
      this.listTen.splice(i,1)
        
    }
  }

  @Injectable({
    providedIn: 'root'
  })
  export class AnhService {
    API_URL: string = 'https://localhost:44368/api/anhs';

    constructor( public http: HttpClient) { 
      
    }

    getAllAnh(){
         return this.http.get(this.API_URL);
     }
  }
 
  @Injectable({
    providedIn: 'root'
  })
  export class ChitietHoadonService {
    API_URL: string = 'https://localhost:44368/api/chitiethoadons';
    cts:ChiTietHoaDon[]=[];
    table:Ban;
    soluong;
    tongtien;
    constructor( public http: HttpClient) {}
    
    getTable(b:Ban){
      this.table=b;
    }
    add(chitiethoadon){
      return this.http.post(this.API_URL, chitiethoadon,httpOptions);
   }
    getChiTietTheoBan(id:number){
         return this.http.get(`${this.API_URL}/getchitiettheoban/${id}`);
     }
    deleteOrderDetail(id:number){
      return this.http.delete(`${this.API_URL}/${id}`);
    } 
    update(chiTiet:ChiTietHoaDonReq){
      return this.http.put(`${this.API_URL}/${chiTiet.chitiethoadonID}`, chiTiet,httpOptions);
    }
  }

  @Injectable({
    providedIn: 'root'
  })
  export class HoaDonService {
    API_URL: string = 'https://localhost:44368/api/hoadons';

    constructor( public http: HttpClient) { 
      
    }
    add(hoadon: HoaDon){
      return this.http.post(this.API_URL, hoadon,httpOptions);
   }
    getOrderFromTableId(id: number){
      return this.http.get(`${this.API_URL}/getOrderFromTableId/${id}`);
    }
    getAllOrder(){
      return this.http.get(this.API_URL)
    }
    getStatusOrder(id:number){
      return this.http.get(`${this.API_URL}/getStatusOrder/${id}`);
    }
    update(hoadon:HoaDon){
      return this.http.put(`${this.API_URL}/${hoadon.hoadonID}`, hoadon,httpOptions);
    }
  }