import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {PhieuNhap} from '../models/phieunhap.class';
import { ChiTietPhieuNhap } from '../models/chitietphieunhap.class';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ChiTietPhieuNhapService {

  API_URL: string = 'https://localhost:44368/api/chitietphieunhaps';
  public chitietphieunhapItemSelected: ChiTietPhieuNhap = new ChiTietPhieuNhap();

  public listchitietphieunhap:any[] =[];
  constructor( public http: HttpClient) { }


  getAllChiTietPhieuNhapByPhieuNhap(id:number){
   
    return this.http.get(`${this.API_URL}/${id}`);
  }
  add(chitietphieunhap){
    return this.http.post(this.API_URL, chitietphieunhap,httpOptions);
  }
  update(chitietphieunhap: ChiTietPhieuNhap){
    return this.http.put(`${this.API_URL}/${chitietphieunhap.chitietphieunhapID}`, chitietphieunhap,httpOptions);
  }
  delete(id: number){
    return this.http.delete(`${this.API_URL}/${id}`);
  }

}


@Injectable({
  providedIn: 'root'
})
export class PhieuNhapService {
  API_URL: string = 'https://localhost:44368/api/phieunhaps';
  
  public listphieunhap:any[] =[];
  public phieunhapItemSelected: PhieuNhap = new PhieuNhap();
  constructor(public http: HttpClient) { }
  API_URL2 = 'https://localhost:44368/api/phieunhaps';
  getAllPhieuNhap(){
   
    return this.http.get(this.API_URL2);
  }
  add(phieunhap){
    return this.http.post(this.API_URL, phieunhap,httpOptions);
  }
  update(phieunhap: PhieuNhap){
    return this.http.put(`${this.API_URL}/${phieunhap.id}`, phieunhap,httpOptions);
  }
  delete(id: number){
    return this.http.delete(`${this.API_URL}/${id}`);
  }
}
