import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HangHoa} from '../models/hanghoa.class';
import { ThucDon } from '../models/thucdon.class';
import {Http, Response, Headers, RequestOptions, RequestMethod} from '@angular/http';
import {map} from 'rxjs/operators';
import { NhomHang } from '../models/nhomhang.class';
import { MonThem } from '../models/monthem.class';
import { DateRange } from '../models/date-range.class';
@Injectable({
  providedIn: 'root'
})
export class HangHoaService {

  API_URL: string = 'https://localhost:44368/api/hanghoas';
  public hanghoaItemSelected: HangHoa = new HangHoa();
  public listhanghoa:any[] =[];
  constructor( public http: Http) { }


  getHanghoabyID(id:number){
      return this.http.get(`${this.API_URL}/${id}`);
  }

 

  add(hh){
    var body = JSON.stringify(hh);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method:RequestMethod.Post, headers:headerOptions});
    return this.http.post(this.API_URL,body,requestOptions).pipe(map(x=>x.json()));
  }

  update(id, hh){
    var body = JSON.stringify(hh);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method:RequestMethod.Put, headers:headerOptions});
    return this.http.put(`${this.API_URL}/${id}`,
     body, 
     requestOptions).pipe(map(x=>x.json()));
  }

  getAllhanghoa(){
   return this.http.get(this.API_URL);
  }
  delete(id: number){
    return this.http.delete(`${this.API_URL}/${id}`).pipe(map(res=>res.json()));
  }

  getMonTheoNhom(id:number){
    return this.http.get(`${this.API_URL}/getmontheonhom/${id}`);
  }
  getMonTheoTen(id:string){
    return this.http.get(`${this.API_URL}/getmontheoten/${id}`);
  }
  getPriceFromId(id:number){
    return this.http.get(`${this.API_URL}/getPriceFromId/${id}`);
  }
}



@Injectable({
  providedIn: 'root'
})
export class ThucDonService {
  API_URL: string = 'https://localhost:44368/api/thucdons';
  
  public listthucdon:any[] =[];
  constructor( public http: Http) { }
  getAllThucDon(){
   return this.http.get(this.API_URL) .pipe(map((data: Response)=>{
    return data.json() as ThucDon[];
  })).toPromise().then(x=>{
    this.listthucdon = x;
     }) ;
  }
}

@Injectable({
  providedIn: 'root'
})
export class NhomHangService {
  API_URL: string = 'https://localhost:44368/api/nhomhangs';
  
  public listnhomhang:NhomHang[];
  constructor( public http: Http) { 
    
  }
  getAllNhomHang(){
   return this.http.get(this.API_URL) .pipe(map((data: Response)=>{
    return data.json() as NhomHang[];
  })).toPromise().then(x=>{
    this.listnhomhang = x;
    console.log(this.listnhomhang)
     }) ;
  }

}


@Injectable({
  providedIn: 'root'
})
export class MonThemService {
  API_URL: string = 'https://localhost:44368/api/monthems';
  
  public listmonthem:MonThem[]=[];
  constructor( public http: Http) { 
    
  }

  getAllMonThem(id:number){
   return this.http.get(`${this.API_URL}/bymonan/${id}`) .pipe(map((data: Response)=>{
    return data.json() as MonThem[];
  })).toPromise().then(x=>{
    this.listmonthem = x;
    console.log(this.listmonthem);
     }) ;
  }
  getAllMonDuocThem(id:number){
   return this.http.get(`${this.API_URL}/bymonthem/${id}`) .pipe(map((data: Response)=>{
    return data.json() as MonThem[];
  })).toPromise().then(x=>{
    this.listmonthem = x;
    console.log(this.listmonthem);
     }) ;
  }

}


@Injectable({
  providedIn: 'root'
})
export class ThongKeService {
  API_URL: string = 'https://localhost:44368/api/hanghoas';
  
  public listnhomhang:NhomHang[];
  constructor( public http: Http) { 
    
  }
  getThongKeTonKho(daterange:DateRange){
    return this.http.post(`${this.API_URL}/getthongketonkho`,daterange).pipe(map(x=>x.json()));
    
  }
  
  getAllNhomHang(){
   return this.http.get(this.API_URL) .pipe(map((data: Response)=>{
    return data.json() as NhomHang[];
  })).toPromise().then(x=>{
    this.listnhomhang = x;
    console.log(this.listnhomhang)
     }) ;
  }

}

@Injectable({
  providedIn: 'root'
})
export class ThongKeNhapHangService {
  API_URL: string = 'https://localhost:44368/api/hanghoas';
  
  public listnhomhang:NhomHang[];
  constructor( public http: Http) { 
    
  }
  getThongKeNhapHang(daterange:DateRange){
    return this.http.post(`${this.API_URL}/getthongkenhaphang`,daterange).pipe(map(x=>x.json()));
    
  }
  
  getAllNhomHang(){
   return this.http.get(this.API_URL) .pipe(map((data: Response)=>{
    return data.json() as NhomHang[];
  })).toPromise().then(x=>{
    this.listnhomhang = x;
    console.log(this.listnhomhang)
     }) ;
  }

}