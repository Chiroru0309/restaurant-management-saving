import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PhongBanComponent} from './phong-ban.component';
import { PhongBanRoutingModule } from './phong-ban-routing.module';
import { GridModule } from '../../../../node_modules/@progress/kendo-angular-grid';
import {FormsModule} from '@angular/forms';
import { PopoverModule} from 'ngx-bootstrap/popover';
import { TabsModule} from 'ngx-bootstrap/tabs';
import { BanDetailComponent } from './ban-detail/ban-detail.component';
import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';

@NgModule({
  imports: [
    CommonModule,
    GridModule,
    PhongBanRoutingModule,
    FormsModule,
    TabsModule.forRoot(),
    PopoverModule.forRoot(),
    LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.threeBounce,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)', 
      primaryColour: '#ff6358', 
      secondaryColour: '#ff6358', 
      tertiaryColour: '#ff6358'
  })
  ],
  providers:[],
  declarations: [PhongBanComponent, BanDetailComponent]
})
export class PhongBanModule { }
