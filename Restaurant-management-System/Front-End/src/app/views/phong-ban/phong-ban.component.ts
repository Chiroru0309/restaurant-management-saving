import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { HttpClient } from '../../../../node_modules/@angular/common/http';
import {NgForm} from '@angular/forms';
//import model ban
import { Ban } from '../models/ban.class';
//import kendo UI
import { process, State } from '@progress/kendo-data-query';
import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
//import service
import {PhongbanService} from '../shared/phongban.service';
//import toastr
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-phong-ban',
  templateUrl: './phong-ban.component.html',
  styleUrls: ['./phong-ban.component.scss']
})

export class PhongBanComponent implements OnInit,AfterViewInit {
  public gridData: GridDataResult;
  public loading = false;
  public state: State = {
    skip: 0,
    take: 5,
  };
  public bans: Ban[];
  public nhombans: Ban[];
  public data: Object[];

  @ViewChild(GridComponent) grid: GridComponent;
  
  constructor(private http: HttpClient, private service: PhongbanService, private toastr: ToastrService) { }

  ngOnInit() {
    this.loadData();
    this.getnhomban();
  }

  //order 1 trang 
  pageList: number[] = [5,10,20,50,100];
  onPageSelectedChange(val: any){
    this.state.take = val;
    this.loadData();
   }

  public ngAfterViewInit(): void {
    //mở rộng 1 hàng
    //this.grid.expandRow(0);
  }
  //thay đổi dữ liệu khi chuyển trang
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state=state;
    //reload dữ liệu
    this.loadData();
  }
  //load dữ liệu 
  loadData(): any {
    this.loading=!this.loading;
    this.service.getAllPhongBan().subscribe((data: Ban[])=>{
          this.bans=data;
          this.bans.reverse();
          this.gridData =  process(this.bans, this.state);
          this.loading=!this.loading;
        });
  }


  //hàm xóa bàn
  deleteRow(){
    this.service.delete(this.service.banItemSelected.banID).subscribe((data: Ban)=> {
      this.toastr.error('Xóa thành công!','Xóa phòng/bàn');
      this.loadData();
    });
    
  }
  //reset thông tin bàn đã được chọn
  resetItemSelected(){

    this.service.banItemSelected = {
      banID:null,
      tenban: '',
      nhombanID:-1,
      soghe:0,
      ghichu:'',
      trangthai: true
    };
  }
  //dừng hoạt động 1 bàn
  stopActionRow(){
    this.service.banItemSelected.trangthai = !this.service.banItemSelected.trangthai
    console.log(this.service.banItemSelected.trangthai);
     this.service.update(this.service.banItemSelected).subscribe((data: Ban)=>{
      this.toastr.warning('Cập nhật trạng thái thành công!','Trạng thái của phòng/bàn');
      this.loadData();
     });
    
    }
    //cập nhật hoặc thêm dữ liệu
    onSubmit(banFrom: NgForm){
      if(banFrom.value.banID==null){
          delete banFrom.value.banID;
          this.service.add(banFrom.value).subscribe((data:Ban)=>
        {
         console.log(banFrom.value);
          this.toastr.success("Thêm mới thành công!","Thêm mới phòng/bàn");
          this.loadData();
        });
      }
      else{
        this.service.update(banFrom.value).subscribe((data:Ban)=>
        {
        
          this.toastr.info('Cập nhật thành công!','Cập nhật phòng/bàn');
          this.loadData();
        });
      }
    }

    getnhomban(){
      this.service.getAllNhomBan().subscribe((data:Ban[])=>{
       this.nhombans=data;
      }
    );
    }
}
