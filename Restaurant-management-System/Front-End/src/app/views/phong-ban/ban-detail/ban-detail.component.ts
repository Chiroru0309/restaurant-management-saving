import { Component, OnInit, Input } from '@angular/core';
import { Ban } from '../../models/ban.class';
import { PhongbanService } from '../../shared/phongban.service';

@Component({
  selector: 'ban-detail',
  templateUrl: './ban-detail.component.html',
  styleUrls: ['./ban-detail.component.scss']
})
export class BanDetailComponent implements OnInit {
@Input() dataItem: Ban;
@Input() nhombans: Ban[];
  constructor(private service:PhongbanService) { }

  ngOnInit() {
  }
  //lấy thông tin bàn được chọn
  getBanByIndex(dataItem){
    this.service.titleSelected=false;
    this.service.banItemSelected = dataItem;
  }
}
