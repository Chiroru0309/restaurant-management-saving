import { PhieuNhap } from "./phieunhap.class";
import { HangHoa } from "./hanghoa.class";

export class ChiTietPhieuNhap{
    chitietphieunhapID: number;
    phieunhapID :number;
    soluong: number;
    dongia: number;
    mahanghoa:string;
    giamgia: number;
    phieuNhap: PhieuNhap[];
    hangHoa: HangHoa[];
}