
export class DateRange{
    startdate: Date;
    enddate: Date;

    constructor(value1:Date, value2:Date){
        this.startdate=value1;
        this.enddate=value2;
    }
}