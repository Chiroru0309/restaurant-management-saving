export class PhieuNhap{
    id: number;
    maphieunhap: string;
    thoigian: Date;
    nhacungcap: string;
    ghichu:string;
    giamgia: number;
    datra: number;
    trangthai: boolean;
}