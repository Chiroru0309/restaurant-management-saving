import { HangHoa } from "./hanghoa.class";
import { HoaDon } from "./hoadonreq.class";

export class ChiTietHoaDon {
    public chitiethoadonID: number;
    public hangHoa: HangHoa;
    public hanghoaID=this.hangHoa.hanghoaID
    public hoaDon:HoaDon;
    public hoadonID=this.hoaDon.hoadonID;
    public soluong: number;
    public ghichu: string;
    public giamgia: any;
}