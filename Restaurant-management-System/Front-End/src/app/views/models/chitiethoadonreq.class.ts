export class ChiTietHoaDonReq {
    public chitiethoadonID: number;
    public hanghoaID: number;
    public hoadonID: number;
    public soluong: number;
    public ghichu: string;
    public giamgia: any;
}