export class NhapHang{
    id: number;
    maphieunhap: string;
    thoigian: Date;
    nhacungcap: string;
    nguoitao: string;
    trangthai: boolean;
    chinhanh: string;
    nguoinhap: string;
    ghichu:string;
    mahang: string;
    tenhang: string;
    soluong: number;
    dongia: number;
    giamgia: number;
    thanhtien: number;
    tongtienhang: number;
    tonggiamgia:number;
    tongcong: number;
    datra: number
}