import { ThucDon } from "./thucdon.class";
import { NhomHang } from "./nhomhang.class";
import { LoaiHang } from "./loaihang.class";

export class HangHoa{
    hanghoaID: number;
    mahanghoa: string;
    tenhang: string;
    giavon: number;
    giaban: number;
    tonkho: number;
    dathang: number;
    dmin: number;
    dmnn: number;
    mota:string;
    ghichu:string;
    lamonthem:boolean;
    hinhthucban:boolean;
    trangthai: boolean;
    thucDon: ThucDon;
    nhomHang: NhomHang
    loaihang: LoaiHang
}