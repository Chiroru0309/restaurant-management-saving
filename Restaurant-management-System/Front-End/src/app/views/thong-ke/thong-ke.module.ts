import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ThongKeComponent} from'./thong-ke.component';
import { ThongKeRoutingModule } from './thong-ke-routing.module';
import {FormsModule} from '@angular/forms';
import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';
import { GridModule } from '../../../../node_modules/@progress/kendo-angular-grid';
import{DatePickerModule} from '@progress/kendo-angular-dateinputs';

@NgModule({
  imports: [
    CommonModule,
    ThongKeRoutingModule,
    FormsModule,
    GridModule,
    DatePickerModule,
    LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.threeBounce,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)', 
      primaryColour: '#ff6358', 
      secondaryColour: '#ff6358', 
      tertiaryColour: '#ff6358'
  })
  ],
  declarations: [ThongKeComponent],
 
})
export class ThongKeModule { }
