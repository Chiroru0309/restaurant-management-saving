import { Component, OnInit } from '@angular/core';
import { ThongKeService } from '../shared/hoanghoa.service';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { DateRange } from '../models/date-range.class';
import { map } from 'rxjs/operators';
import { Response } from '@angular/http';
import { TonKho } from '../models/tonkho.class';

@Component({
  selector: 'app-thong-ke',
  templateUrl: './thong-ke.component.html',
  styleUrls: ['./thong-ke.component.scss']
})
export class ThongKeComponent implements OnInit {
  public gridData: any[] = [];
  public loading = false;
  public dataResult: any;
  constructor(private thongkeservice: ThongKeService) { }

  ngOnInit() {
  }
  public value1: Date = new Date(2000, 2, 10);
  public value2: Date = new Date(2000, 2, 10);

  xemthongtinthongke() {
    this.loading = true;
    const daterange = new DateRange(this.value1, this.value2);
    this.thongkeservice.getThongKeTonKho(daterange).pipe(map((data: Response) => {
      return data.json() as TonKho[];
    })).toPromise().then(x => {
      this.gridData = x;
      this.loading = !this.loading;
    });
  }

}
