import { NgModule } from '@angular/core';
import { GridModule, ExcelModule, PDFModule } from '../../../../node_modules/@progress/kendo-angular-grid';
import {FormsModule} from '@angular/forms';
import { PhieuNhapRoutingModule } from './phieunhap-routing.module';
import { PhieunhapComponent } from './phieunhap.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';
import { ChitietphieunhapComponent } from './chitietphieunhap/chitietphieunhap.component';
@NgModule({
  imports: [
    PhieuNhapRoutingModule,
    GridModule,
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    AccordionModule.forRoot(),
    FormsModule,
    ExcelModule,
    PDFModule ,
    LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.wanderingCubes,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)', 
      backdropBorderRadius: '4px',
      primaryColour: '#ffffff', 
      secondaryColour: '#ffffff', 
      tertiaryColour: '#ffffff'
  })

  ],
  declarations: [ PhieunhapComponent, ChitietphieunhapComponent ]
})
export class PhieuNhapModule { }