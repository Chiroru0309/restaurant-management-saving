import { Component, OnInit, ViewChild, AfterViewInit,TemplateRef } from '@angular/core';
import { HttpClient } from '../../../../node_modules/@angular/common/http';
import {NgForm} from '@angular/forms';
//import model ban
import { ChiTietPhieuNhap } from '../models/chitietphieunhap.class';
//import kendo UI
import { process, State } from '@progress/kendo-data-query';
import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
//import service
import {PhieuNhapService} from '../shared/phieunhap.service';
//import toastr
import { ToastrService } from 'ngx-toastr';

//import ngx-modal
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { PhieuNhap } from '../models/phieunhap.class';
import { getNow } from '@progress/kendo-angular-dateinputs/dist/es2015/util';

@Component({
  selector: 'app-phieunhap',
  templateUrl: './phieunhap.component.html',
  styleUrls: ['./phieunhap.component.scss']
})
export class PhieunhapComponent implements OnInit, AfterViewInit {
  public gridData: GridDataResult;
  public gridDataDetails: GridDataResult;
  public loading = false;
  public state: State = {
    skip: 0,
    take: 5,
  };
  public phieunhaps: PhieuNhap[];
  public chitietphieunhaps: ChiTietPhieuNhap[];
  public data: Object[];
  @ViewChild(GridComponent) grid: GridComponent;
  
  constructor(private http: HttpClient,private servicephieunhap: PhieuNhapService,  private toastr: ToastrService,private modalService: BsModalService) { }

 
  ngOnInit() {
    this.resetItemSelected();
    this.loadData();
  }
  public ngAfterViewInit(): void {
    //mở rộng 1 hàng
    //this.grid.expandRow(0);
  }

  
  //thay đổi dữ liệu khi chuyển trang
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state=state;
    //reload dữ liệu
    this.loadData();
  }
  loadData(): any {
    this.loading=true;
    this.servicephieunhap.getAllPhieuNhap().subscribe((data:PhieuNhap[])=>{
      this.phieunhaps = data;
      this.phieunhaps.reverse();
      this.servicephieunhap.listphieunhap= this.phieunhaps;
          this.gridData =  process(this.phieunhaps, this.state);
          this.loading=false;
    });
  }
  
  //lấy thông tin hanghoa được chọn
  getPhieuNhapSelected(dataItem){
    //this.servicephieunhap.titleSelected=false;
    this.servicephieunhap.phieunhapItemSelected = dataItem;
  }

 

  //reset thông tin bàn đã được chọn
  resetItemSelected(){
    this.servicephieunhap.phieunhapItemSelected = {
      id: null,
      maphieunhap: '',
      thoigian: getNow(),
      nhacungcap: '',
      ghichu:'',
      giamgia: 0,
      datra: 0,
      trangthai: true
    };
  }

  //dừng hoạt động 1 bàn
  stopActionRow(){
    this.servicephieunhap.phieunhapItemSelected.trangthai = !this.servicephieunhap.phieunhapItemSelected.trangthai
    console.log(this.servicephieunhap.phieunhapItemSelected.trangthai);
     this.servicephieunhap.update(this.servicephieunhap.phieunhapItemSelected).subscribe(()=>{
      this.toastr.warning('Cập nhật trạng thái thành công!','Trạng thái của phiếu nhập hàng');
      this.modalRef.hide();
      this.loadData();
     });
    
    }

    //cập nhật hoặc thêm dữ liệu
    onSubmit(){
        // this.servicephieunhap.update(this.servicephieunhap.phieunhapItemSelected).subscribe(()=>
        // {
        //   this.modalRef.hide();        
        //   this.toastr.info('Cập nhật thành công!','Cập nhật phiếu nhập hàng');
        //   this.loadData();
        // });
      
    }

    //modal confirm
    modalRef: BsModalRef;
    openModal(template: TemplateRef<any>) {
      this.modalRef = this.modalService.show(template, {class: 'gray modal-lg'});
    }
    //hàm xóa bàn

    confirm(): void {
      // this.service.delete(this.service.phieunhapItemSelected.id).subscribe(()=> {
      //   this.toastr.error('Xóa thành công!','Xóa phiếu nhập hàng');
      //   this.modalRef.hide();
      //   this.loadData();
      // });
     
    }
   
    decline(): void {
      this.modalRef.hide();
    }
}
