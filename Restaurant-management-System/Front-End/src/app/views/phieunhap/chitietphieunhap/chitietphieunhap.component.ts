import { Component, OnInit, Input, ViewChild } from '@angular/core';
//import kendo UI
import { process, State } from '@progress/kendo-data-query';
import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import { ChiTietPhieuNhap } from '../../models/chitietphieunhap.class';
import { ChiTietPhieuNhapService } from '../../shared/phieunhap.service';
@Component({
  selector: 'chitietphieunhap',
  templateUrl: './chitietphieunhap.component.html',
  styleUrls: ['./chitietphieunhap.component.scss']
})
export class ChitietphieunhapComponent implements OnInit {
  public gridDataDetails: GridDataResult;
  public loading = false;
  public state: State = {
    skip: 0,
    take: 5,
  };
  public chitietphieunhaps: ChiTietPhieuNhap[];
  @Input() dataItem:ChiTietPhieuNhap
  @ViewChild(GridComponent) grid: GridComponent;
  constructor(private service: ChiTietPhieuNhapService) { }

  ngOnInit() {
    this.loadDataChiTiet();
  }

   //thay đổi dữ liệu khi chuyển trang
   public dataStateChange(state: DataStateChangeEvent): void {
    this.state=state;
    //reload dữ liệu
    this.loadDataChiTiet();
  }
//load dữ liệu 
  loadDataChiTiet(): any {
    this.loading=true;
    this.service.getAllChiTietPhieuNhapByPhieuNhap(this.dataItem.chitietphieunhapID).subscribe((data:ChiTietPhieuNhap[])=>{
      this.chitietphieunhaps = data;
      this.service.listchitietphieunhap= this.chitietphieunhaps;
          this.gridDataDetails = process(this.chitietphieunhaps, this.state);
          this.loading=false;
      })   ;
  }
}
