import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PhieunhapComponent } from './phieunhap.component';

const routes: Routes = [
  {
    path: '',
    component: PhieunhapComponent,
    data: {
      title: 'PhieuNhap'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhieuNhapRoutingModule {}
