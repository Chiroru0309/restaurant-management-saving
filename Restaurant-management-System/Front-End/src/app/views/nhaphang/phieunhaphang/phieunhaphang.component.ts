import { Component, OnInit } from '@angular/core';
import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import { ToastrService } from 'ngx-toastr';
import { HanghoaComponent } from '../../hanghoa/hanghoa.component';
import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';
import { NhaphanghoaComponent } from '../nhaphanghoa/nhaphanghoa.component';
import { NhapHangService } from '../../shared/nhaphang.service';

@Component({
  selector: 'app-phieunhaphang',
  templateUrl: './phieunhaphang.component.html',
  styleUrls: ['./phieunhaphang.component.scss']
})
export class PhieunhaphangComponent implements OnInit {

  constructor(private nhaphangService:NhapHangService ) { }
  nhaphanghoaComponent: NhaphanghoaComponent;
  ngOnInit() {
    console.log(this.nhaphangService.tongtien)
  }
  giamgia:number = 0;
  datra:number = 0;

}
