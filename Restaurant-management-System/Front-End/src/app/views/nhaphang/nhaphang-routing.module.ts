import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NhaphangComponent } from './nhaphang.component';

const routes: Routes = [
  {
    path: '',
    component: NhaphangComponent,
    data: {
      title: 'NhapHang'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NhapHangRoutingModule {}
