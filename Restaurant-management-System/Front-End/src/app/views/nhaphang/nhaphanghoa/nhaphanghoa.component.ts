import { Component, OnInit,ViewChild, TemplateRef } from '@angular/core';
import {HangHoaService} from '../../shared/hoanghoa.service';
import { HangHoa } from '../../models/hanghoa.class';
import { NhapHang } from '../../models/nhaphang.class';
import { GridModule } from '../../../../../node_modules//@progress/kendo-angular-grid';
import {FormsModule} from '@angular/forms';
import { PopoverModule} from 'ngx-bootstrap/popover';
import { TabsModule} from 'ngx-bootstrap/tabs';
import {map} from 'rxjs/operators';
import { CommonModule } from '@angular/common';
import { Response } from '@angular/http';
import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import { ToastrService } from 'ngx-toastr';
import { HanghoaComponent } from '../../hanghoa/hanghoa.component';
import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';
import { process, State } from '@progress/kendo-data-query';
import {NgForm} from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {HttpClient} from '@angular/common/http';
import {NhapHangService} from '../../shared/nhaphang.service';
import { ChiTietPhieuNhap } from '../../models/chitietphieunhap.class';
import { PhieuNhapService } from '../../shared/phieunhap.service';
import { PhieuNhap } from '../../models/phieunhap.class';


@Component({
  selector: 'app-nhaphanghoa',
  templateUrl: './nhaphanghoa.component.html',
  styleUrls: ['./nhaphanghoa.component.scss']
})
export class NhaphanghoaComponent implements OnInit {
  public gridData: GridDataResult;
  public loading = false;
  public hanghoas: HangHoa[];
  public phieunhaps:PhieuNhap[];
  public state: State = {
    skip: 0,
    take: 5,
  };
  public nhaphangs: NhapHang[];
  hanghoa:string;
  phieunhap:string;
  @ViewChild(GridComponent) grid: GridComponent;
  
  tongtienhang:number = 0;
  constructor( private http: HttpClient, private service1: NhapHangService,private service2:HangHoaService, private service3:PhieuNhapService, private toastr: ToastrService,private modalService: BsModalService) { }
  ngOnInit() {
     this.service2.getAllhanghoa()
     .pipe(map((data: Response)=>{
       return data.json() as HangHoa[];
     })).toPromise().then(x=>{
       this.hanghoas = x;
       console.log(this.hanghoas)
     }) ;

     this.service3.getAllPhieuNhap()
    .pipe(map((data:Response)=>{
      return data.json() as PhieuNhap[];
    })).toPromise().then(x=>{
      this.phieunhaps = x;
      console.log(this.phieunhaps)
    });
   this.loadData();
   this.service1.getTongtien(this.tongtienhang);
  }
  sohang:number = 0;
  loadData(): any {
    this.loading=true;
    //this.hanghoa = 'SP00002'; // thu nghiem voi ma hang 1
    this.service1.getAllnhaphang().subscribe((data: NhapHang[])=>{
          
          this.nhaphangs=data;
          let tong : number = 0;
          data.forEach(x=>{
            tong  += x.soluong*(x.dongia-x.giamgia);
          })
          this.tongtienhang= tong;
          console.log(this.tongtienhang);
          
          this.nhaphangs.reverse();
          this.gridData =  process(this.nhaphangs, this.state);
          this.loading=false;
        });
    
  }


  bamnut(hh,pn){
    this.service1.add(hh,pn).subscribe((data:NhapHang)=>
        {
        // console.log(dulieu);
          this.toastr.success("Thêm mới thành công!","Thêm mới chi tiết phiếu nhập");
          this.loadData();
        });
        this.loadData();
    // this.loading=true;
    // //this.hanghoa = 'SP00002'; // thu nghiem voi ma hang 1
    // this.service1.getAllnhaphang().subscribe((data: NhapHang[])=>{
          
    //       this.nhaphangs=data;
    //       let tong : number = 0;
    //       data.forEach(x=>{
    //         tong  += x.soluong*(x.dongia-x.giamgia);
    //       })
    //       this.tongtienhang= tong;
    //       console.log(this.tongtienhang);
          
    //       this.nhaphangs.reverse();
    //       this.gridData =  process(this.nhaphangs, this.state);
    //       this.loading=false;
    //     });
    // this.service1.getTongtien(this.tongtienhang);
  }
  bienNhaphang: NhapHang;
  //this.bienNhaphang = this.service1.getNhaphangById(1);
  Onsubmit(){
    
    this.service1.add(this.hanghoa).subscribe((data:NhapHang)=>
        {
        // console.log(dulieu);
          this.toastr.success("Thêm mới thành công!","Thêm mới phòng/bàn");
          this.loadData();
        });
  }

  public tangsoluong(dulieu)
  {
    dulieu.soluong++;
    //alert(dulieu.phieuNhap.id + ' ' + dulieu.ChiTietPhieuNhap.chitietphieunhapid +' '+ dulieu.hangHoa.mahanghoa + ' '+ dulieu.hangHoa.tenhang + ' ' + dulieu.nhacungcap + ' ' + dulieu.soluong + ' ' + dulieu.tenhang + ' ' + dulieu.dongia + ' ' + dulieu.giamgia);
    //alert(dulieu.chitietphieunhapID +' '+ dulieu.phieuNhap.maphieunhap);
    //let dulieuthaydoi:ChiTietPhieuNhap;
    //dulieuthaydoi.chitietphieunhapID = dulieu.chitietphieunhapID;
    //dulieuthaydoi.dongia = dulieu.dongia;
    //dulieuthaydoi.giamgia = dulieu.giamgia;
    //dulieuthaydoi.soluong = dulieu.soluong;
    //dulieuthaydoi.phieuNhap.
    dulieu.mahanghoa = dulieu.hangHoa.mahanghoa;
    dulieu.phieunhapID = dulieu.phieuNhap.id;
    let tong : number = 0;
          tong+= dulieu.dongia - dulieu.giamgia
          this.tongtienhang += tong;
    this.service1.update(dulieu).subscribe((data:ChiTietPhieuNhap)=>
    {
      
      console.log(dulieu);
      this.toastr.success("Số lượng hàng vừa tăng thêm","cập nhật thành công");
      this.loadData();
    });
    this.service1.getTongtien(this.tongtienhang);
  }

  public giamsoluong(dulieu)
  {
    dulieu.soluong--;
    //alert(dulieu.phieuNhap.id + ' ' + dulieu.ChiTietPhieuNhap.chitietphieunhapid +' '+ dulieu.hangHoa.mahanghoa + ' '+ dulieu.hangHoa.tenhang + ' ' + dulieu.nhacungcap + ' ' + dulieu.soluong + ' ' + dulieu.tenhang + ' ' + dulieu.dongia + ' ' + dulieu.giamgia);
    //alert(dulieu.chitietphieunhapID +' '+ dulieu.phieuNhap.maphieunhap);
    //let dulieuthaydoi:ChiTietPhieuNhap;
    //dulieuthaydoi.chitietphieunhapID = dulieu.chitietphieunhapID;
    //dulieuthaydoi.dongia = dulieu.dongia;
    //dulieuthaydoi.giamgia = dulieu.giamgia;
    //dulieuthaydoi.soluong = dulieu.soluong;
    //dulieuthaydoi.phieuNhap.
    dulieu.mahanghoa = dulieu.hangHoa.mahanghoa;
    dulieu.phieunhapID = dulieu.phieuNhap.id;
    
    let tong : number = 0;
          tong+= dulieu.dongia - dulieu.giamgia
          this.tongtienhang -= tong;

    this.service1.update(dulieu).subscribe((data:ChiTietPhieuNhap)=>
    {
      
      console.log(dulieu);
      this.toastr.success("Số lượng hàng vừa giảm đi","cập nhật thành cống");
      this.loadData();
    });
    this.service1.getTongtien(this.tongtienhang);
  }

 //id = 0;

 // loadData(): any {
    // this.loading=true;
    // this.service2.getAllhanghoa().subscribe((data: Hanghoa[])=>{
    //       this.hanghoas=data;
    //       this.hanghoas.reverse();
    //       this.gridData =  process(this.hanghoas, this.state);
    //       this.loading=false;
    //     });
    // this.loading=true;
    // for(this.id =0;this.id<9999;this.id++)
    // {
    //   this.service2.getAllhanghoa().subscribe((data:HangHoa[]))
    // }
    
 // }
  
}

