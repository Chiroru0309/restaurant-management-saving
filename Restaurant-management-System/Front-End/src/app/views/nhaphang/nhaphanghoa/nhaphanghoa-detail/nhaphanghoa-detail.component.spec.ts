import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NhaphanghoaDetailComponent } from './nhaphanghoa-detail.component';

describe('NhaphanghoaDetailComponent', () => {
  let component: NhaphanghoaDetailComponent;
  let fixture: ComponentFixture<NhaphanghoaDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NhaphanghoaDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NhaphanghoaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
