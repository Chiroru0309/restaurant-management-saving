import { Component, OnInit, ViewChild, AfterViewInit,TemplateRef } from '@angular/core';
import { HttpClient } from '../../../../node_modules/@angular/common/http';
import {NgForm} from '@angular/forms';
//import model ban
import { NhapHang } from '../models/nhaphang.class';
//import kendo UI
import { process, State } from '@progress/kendo-data-query';
import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
//import service
import {NhapHangService} from '../shared/nhaphang.service';

//import toastr
import { ToastrService } from 'ngx-toastr';

//import ngx-modal
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { getToday } from '@progress/kendo-angular-dateinputs/dist/es2015/util';

@Component({
  selector: 'app-nhaphang',
  templateUrl: './nhaphang.component.html',
  styleUrls: ['./nhaphang.component.scss']
})
export class NhaphangComponent implements OnInit, AfterViewInit {
  public gridData: GridDataResult;
  public loading = false;
  public state: State = {
    skip: 0,
    take: 5,
  };
  public nhaphangs: NhapHang[];
  public data: Object[];
  @ViewChild(GridComponent) grid: GridComponent;
  constructor(private http: HttpClient, private service: NhapHangService, private toastr: ToastrService,private modalService: BsModalService) { }

 
  ngOnInit() {
    this.resetItemSelected();
    this.loadData();
  }
  public ngAfterViewInit(): void {
    //mở rộng 1 hàng
    //this.grid.expandRow(0);
  }

  
  //thay đổi dữ liệu khi chuyển trang
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state=state;
    //reload dữ liệu
    this.loadData();
  }
  //load dữ liệu 
  loadData(): any {
    this.loading=true;
    this.service.getAllnhaphang().subscribe((data: NhapHang[])=>{
          this.nhaphangs=data;
          this.nhaphangs.reverse();
          this.gridData =  process(this.nhaphangs, this.state);
          this.loading=false;
        });
  }
  //lấy thông tin hanghoa được chọn
  getPhieuNhapSelected(dataItem){
    //this.service.titleSelected=false;
    this.service.nhaphangItemSelected = dataItem;
  }

 

  //reset thông tin bàn đã được chọn
  resetItemSelected(){
    this.service.nhaphangItemSelected = {
      id: null,
      maphieunhap: '',
      thoigian: getToday(),
      nhacungcap: '',
      nguoitao: '',
      trangthai: true,
      chinhanh: '',
      nguoinhap: '',
      ghichu:'',
      mahang: '',
      tenhang: '',
      soluong: 0,
      dongia: 0,
      giamgia: 0,
      thanhtien: 0,
      tongtienhang: 0,
      tonggiamgia:0,
      tongcong: 0,
      datra: 0
    };
  }

  //dừng hoạt động 1 bàn
  // stopActionRow(){
  //   this.service.nhaphangItemSelected.trangthai = !this.service.nhaphangItemSelected.trangthai
  //   console.log(this.service.nhaphangItemSelected.trangthai);
  //    this.service.update(this.service.nhaphangItemSelected).subscribe((data: NhapHang)=>{
  //     this.toastr.warning('Cập nhật trạng thái thành công!','Trạng thái của phiếu nhập hàng');
  //     this.modalRef.hide();
  //     this.loadData();
  //    });
    
  //   }

  //   //cập nhật hoặc thêm dữ liệu
  //   onSubmit(){
  //       this.service.update(this.service.nhaphangItemSelected).subscribe((data:NhapHang)=>
  //       {
  //         this.modalRef.hide();        
  //         this.toastr.info('Cập nhật thành công!','Cập nhật phiếu nhập hàng');
  //         this.loadData();
  //       });
      
  //   }

    //modal confirm
    modalRef: BsModalRef;
    openModal(template: TemplateRef<any>) {
      this.modalRef = this.modalService.show(template, {class: 'gray modal-lg'});
    }
    //hàm xóa bàn

    confirm(): void {
      this.service.delete(this.service.nhaphangItemSelected.id).subscribe((data: NhapHang)=> {
        this.toastr.error('Xóa thành công!','Xóa phiếu nhập hàng');
        this.modalRef.hide();
        this.loadData();
      });
     
    }
   
    decline(): void {
      this.modalRef.hide();
    }
}
