import { NgModule } from '@angular/core';
import { GridModule } from '../../../../node_modules/@progress/kendo-angular-grid';
import {FormsModule} from '@angular/forms';
import { NhapHangRoutingModule } from './nhaphang-routing.module';
import { NhaphangComponent } from './nhaphang.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import {TypeaheadModule} from 'ngx-bootstrap/typeahead'
import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';
import { NhaphanghoaComponent } from './nhaphanghoa/nhaphanghoa.component';
import { PhieunhaphangComponent } from './phieunhaphang/phieunhaphang.component';
import { NhaphanghoaDetailComponent } from './nhaphanghoa/nhaphanghoa-detail/nhaphanghoa-detail.component';
import { CommonModule } from '@angular/common';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { HangHoaRoutingModule } from '../hanghoa/hanghoa-routing.module';
import { HanghoaComponent } from '../hanghoa/hanghoa.component';
@NgModule({
  imports: [
    NhapHangRoutingModule,
    CommonModule,
    GridModule,
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    AccordionModule.forRoot(),
    FormsModule,
    PopoverModule.forRoot(),
    TabsModule.forRoot(),
    LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.wanderingCubes,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)', 
      backdropBorderRadius: '4px',
      primaryColour: '#ffffff', 
      secondaryColour: '#ffffff', 
      tertiaryColour: '#ffffff'
  }),
  TypeaheadModule.forRoot(),

  ],
  declarations: [ NhaphangComponent, NhaphanghoaComponent, PhieunhaphangComponent, NhaphanghoaDetailComponent ]
})
export class NhapHangModule { }