import { Component, OnInit } from '@angular/core';
import { ThongKeNhapHangService } from '../shared/hoanghoa.service';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { DateRange } from '../models/date-range.class';
import { map } from 'rxjs/operators';
import { Response } from '@angular/http';
import { BaoCaoNhapHang } from '../models/baocaonhaphang.class';

@Component({
  selector: 'app-bao-cao-nhap-hang',
  templateUrl: './baocaonhaphang.component.html',
  styleUrls: ['./baocaonhaphang.component.scss']
})
export class BaoCaoNhapHangComponent implements OnInit {
  public gridData: any[] = [];
  public loading = false;
  public dataResult: any;
  constructor(private thongkenhaphangservice: ThongKeNhapHangService) { }

  ngOnInit() {
  }
  public value1: Date = new Date(2000, 2, 10);
  public value2: Date = new Date(2000, 2, 10);

  xemthongkenhaphang() {
    this.loading = true;
    const daterange = new DateRange(this.value1, this.value2);
    this.thongkenhaphangservice.getThongKeNhapHang(daterange).pipe(map((data: Response) => {
      return data.json() as BaoCaoNhapHang[];
    })).toPromise().then(x => {
      this.gridData = x;
      this.loading = !this.loading;
    });
  }

}
