import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BaoCaoNhapHangComponent } from './baocaonhaphang.component';

const routes: Routes = [
  {
    path: '',
    component: BaoCaoNhapHangComponent,
    data: {
      title: 'Thống kê nhập hàng'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BaoCaoNhapHangRoutingModule {}