import { Component, OnInit } from '@angular/core';
import { BanCashService, ChitietHoadonService, HoaDonService } from '../../shared/bancash.service';
import { ChiTietHoaDonReq } from '../../models/chitiethoadonreq.class';
import { HangHoaService } from '../../shared/hoanghoa.service';
import { HangHoa } from '../../models/hanghoa.class';
import { map } from 'rxjs/operators';
import { HoaDon } from '../../models/hoadonreq.class';
import { ChiTietHoaDon } from '../../models/ChiTietHoaDon.class';

@Component({
  selector: 'app-orcash',
  templateUrl: './orcash.component.html',
  styleUrls: ['./orcash.component.scss']
})
export class OrcashComponent implements OnInit {

  constructor(private bancashService: BanCashService,
    private chitiethoadonservic: ChitietHoadonService,
    private hangHoaService: HangHoaService,
    private orderService: HoaDonService
  ) {

  }
  ngOnInit() {
  }
  delOrder(i: number, b: any) {
    this.bancashService.delOrder(i, b);
  }
  
  chitiethoadon:ChiTietHoaDonReq={
    chitiethoadonID:null,
    ghichu:null,
    giamgia:0,
    hanghoaID:1,
    hoadonID:1,
    soluong:1
  }
  updateQuantity(detail:ChiTietHoaDon,quantity:number){
    this.chitiethoadon.chitiethoadonID=detail.chitiethoadonID
    this.chitiethoadon.hanghoaID=detail.hanghoaID
    this.chitiethoadon.hoadonID=detail.hoadonID
    this.chitiethoadon.soluong=quantity

    this.chitiethoadonservic.update(this.chitiethoadon).subscribe(()=>{
      this.chitiethoadonservic.getChiTietTheoBan(this.chitiethoadonservic.table.banID).subscribe((data: ChiTietHoaDon[]) => {
        this.chitiethoadonservic.cts = data
        this.chitiethoadonservic.soluong = 0
        this.chitiethoadonservic.tongtien = 0
        this.chitiethoadonservic.cts.forEach(x => {
          this.chitiethoadonservic.soluong += x.soluong
          this.chitiethoadonservic.tongtien+=x.soluong*x.hangHoa.giaban
          // this.hangHoaService.getPriceFromId(x.hanghoaID).subscribe(data=>{
          //   this.chitiethoadonservic.tongtien+=data
          // })
        });
      })
      alert("Cap nhap thanh cong")
    })
  }

  deleteDetail(b: ChiTietHoaDonReq) {
    // alert(b.chitiethoadonID)
    this.chitiethoadonservic.deleteOrderDetail(b.chitiethoadonID).subscribe(() => {
      this.chitiethoadonservic.getChiTietTheoBan(this.chitiethoadonservic.table.banID).subscribe((data: ChiTietHoaDon[]) => {
        this.chitiethoadonservic.cts = data
        this.chitiethoadonservic.soluong = 0
        this.chitiethoadonservic.tongtien = 0
        this.chitiethoadonservic.cts.forEach(x => {
          this.chitiethoadonservic.soluong += x.soluong
          this.chitiethoadonservic.tongtien+=x.soluong*x.hangHoa.giaban
          // this.hangHoaService.getPriceFromId(x.hanghoaID).subscribe(data=>{
          //   this.chitiethoadonservic.tongtien+=data
          // })
        });
        alert("xoa thanh cong");
      })

    })
  }

  hoadon:HoaDon={
    hoadonID:null,
    giamgia:0,
    banID:1,
    PhuongThucThanhToanId:1,
    ngaylap:null,
    tiendatra:0,  
    tinhtrang:false
};

  updateOrder(){
    this.orderService.getOrderFromTableId(this.bancashService.ban.banID).subscribe((data:number)=>{
      this.hoadon.hoadonID=data
      this.hoadon.banID=this.bancashService.ban.banID

      var dte = new Date();
      dte.setDate(dte.getDate())
      this.hoadon.ngaylap=dte

      this.hoadon.tinhtrang=true;
      this.orderService.update(this.hoadon).subscribe(()=>{
        alert("Thanh toan thanh cong")
        this.chitiethoadonservic.cts=null
      })
    })
  }
}
