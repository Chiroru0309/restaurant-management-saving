import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrcashComponent } from './orcash.component';

describe('OrcashComponent', () => {
  let component: OrcashComponent;
  let fixture: ComponentFixture<OrcashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrcashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrcashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
