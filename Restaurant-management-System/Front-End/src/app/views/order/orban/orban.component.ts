import { Component, OnInit, Injectable } from '@angular/core';
import { HttpClient } from '../../../../../node_modules/@angular/common/http';
//import model ban
import { Ban } from '../../models/ban.class';
//import service
import {PhongbanService} from '../../shared/phongban.service';
import { BanCashService, ChitietHoadonService, HoaDonService } from '../../shared/bancash.service';
import { HoaDon } from '../../models/hoadonreq.class';
import { ChiTietHoaDonReq } from '../../models/chitiethoadonreq.class';
import { ChiTietHoaDon } from '../../models/ChiTietHoaDon.class';

@Component({
  selector: 'app-orban',
  templateUrl: './orban.component.html',
  styleUrls: ['./orban.component.scss']
})
export class OrbanComponent implements OnInit {
  public loading = false;
  constructor(private http: HttpClient,
     private service: PhongbanService,
    private bancashService:BanCashService,
    private chitiethoadon: ChitietHoadonService,
    private hoadonService: HoaDonService
   ) { 
    // appState.publish({data: 'some data'});
  }

sotrangs:number[];
bans: Ban[];
public nhombans: Ban[];
ban_sub:any[];
indexPhongBan:number = 0;
cts:ChiTietHoaDonReq[];
flagAdd:boolean;

  ngOnInit() {
    this.loadData();
    this.getnhomban();
  }

nextPhongBan(){
  this.indexPhongBan++;
  
  if ((this.bans.length/12)<=this.indexPhongBan) {
    this.indexPhongBan = 0;
   
  }
  const start:number = this.sotrangs[this.indexPhongBan]*12;
  const end:number = (this.sotrangs[this.indexPhongBan]+1)*12;

  this.ban_sub = this.bans.slice(start,end);
}

previousPhongBan(){
  
  this.indexPhongBan--;

  if (this.indexPhongBan<0) {
    this.indexPhongBan = this.sotrangs[this.sotrangs.length-1];
    

  }
  const start:number = this.sotrangs[this.indexPhongBan]*12;
  const end:number = (this.sotrangs[this.indexPhongBan]+1)*12;
 
  this.ban_sub = this.bans.slice(start,end);
}
//load dữ liệu 
loadData(): any {
  // this.hoadonService.getStatusOrder(3).subscribe((data:boolean)=>this.flagAdd=data)
  this.service.getAllPhongBan().subscribe((data: Ban[])=>{
        this.bans=data; 

        let res = [];
    for (let i = 0; i < this.bans.length/12; i++) {
       res.push(i);
    }
   
    this.sotrangs = res;
    this.ban_sub = this.bans.slice(0,12);
      });

      
}
getnhomban(){
  this.service.getAllNhomBan().subscribe((data:Ban[])=>{
   this.nhombans=data;
  }
);
}
loadBan(value:number){
  this.service.getBanTheoNhom(value).subscribe((data:Ban[])=>{
    this.bans=data;
    let res = [];
    for (let i = 0; i < this.bans.length/12; i++) {
       res.push(i);
    }
   
    this.sotrangs = res;
    this.ban_sub = this.bans.slice(0,12);
   }
 );
}
hoadon:HoaDon={
    hoadonID:null,
    giamgia:0,
    banID:1,
    PhuongThucThanhToanId:1,
    ngaylap:null,
    tiendatra:0,  
    tinhtrang:false
};

sendBan(b:Ban){
  this.bancashService.getTenBan(b);
  this.chitiethoadon.getTable(b);
  this.hoadon.banID=b.banID;
  delete this.hoadon.hoadonID;
  
  this.hoadonService.getStatusOrder(b.banID).subscribe((data:boolean)=>{
    this.flagAdd=data
    if(this.flagAdd==true){
      this.hoadonService.add(this.hoadon).subscribe(()=>{
              alert("them thanh cong");
              
              this.chitiethoadon.getChiTietTheoBan(b.banID).subscribe((data:ChiTietHoaDon[])=>{
                this.chitiethoadon.cts=data
                this.chitiethoadon.soluong=0
                this.chitiethoadon.tongtien = 0
          this.chitiethoadon.cts.forEach(x => {
            this.chitiethoadon.soluong+=x.soluong
            this.chitiethoadon.tongtien+=x.soluong*x.hangHoa.giaban
          });
            })

      })
    }else{
      alert("da co hoa don")
      this.chitiethoadon.getChiTietTheoBan(b.banID).subscribe((data:ChiTietHoaDon[])=>{
          this.chitiethoadon.cts=data
          this.chitiethoadon.soluong=0
          this.chitiethoadon.tongtien = 0
          this.chitiethoadon.cts.forEach(x => {
            this.chitiethoadon.soluong+=x.soluong
            this.chitiethoadon.tongtien+=x.soluong*x.hangHoa.giaban
          });
      })
    }
  });

//  this.chitiethoadon.add(b.banID).subscribe(()=>{
//  });
}
}


