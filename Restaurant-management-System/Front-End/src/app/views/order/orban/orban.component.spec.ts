import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrbanComponent } from './orban.component';

describe('OrbanComponent', () => {
  let component: OrbanComponent;
  let fixture: ComponentFixture<OrbanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrbanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrbanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
