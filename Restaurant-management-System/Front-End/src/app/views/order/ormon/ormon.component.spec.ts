import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrmonComponent } from './ormon.component';

describe('OrmonComponent', () => {
  let component: OrmonComponent;
  let fixture: ComponentFixture<OrmonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrmonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrmonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
