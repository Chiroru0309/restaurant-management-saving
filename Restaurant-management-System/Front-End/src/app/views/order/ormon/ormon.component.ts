import { Component, OnInit } from '@angular/core';
//import service
import {HangHoaService, ThucDonService, NhomHangService,} from '../../shared/hoanghoa.service';
//import model ban
import { HangHoa } from '../../models/hanghoa.class';
import { NhomHang } from '../../models/nhomhang.class';

import { Http,Response } from '@angular/http';
import {map} from 'rxjs/operators';
import { BanCashService, AnhService, ChitietHoadonService, HoaDonService } from '../../shared/bancash.service';
import { Anh } from '../../models/anh.class';
import { ChiTietHoaDonReq } from '../../models/chitiethoadonreq.class';
import { HoaDon } from '../../models/hoadonreq.class';
import { ChiTietHoaDon } from '../../models/ChiTietHoaDon.class';
@Component({
  selector: 'app-ormon',
  templateUrl: './ormon.component.html',
  styleUrls: ['./ormon.component.scss']
})
export class OrmonComponent implements OnInit {

  constructor(
    private service: HangHoaService,
    private myservice: NhomHangService,
    private bancashService: BanCashService,
    private anhService: AnhService,
    private chitietService: ChitietHoadonService,
    private OrderService: HoaDonService
  ) { }
  sotrangs:number[];
  mon_sub:any[];
  indexMon:number = 0;
  public hanghoas: HangHoa[];
  public nhomhangs: NhomHang[];
  ngOnInit() {
    this.loadData();
    this.myservice.getAllNhomHang();
    this.getanh();
  }

nextMon(){
  this.indexMon++;
  
  if ((this.hanghoas.length/12)<=this.indexMon) {
    this.indexMon = 0;
   
  }
  const start:number = this.sotrangs[this.indexMon]*12;
  const end:number = (this.sotrangs[this.indexMon]+1)*12;

  this.mon_sub = this.hanghoas.slice(start,end);
}

previousMon(){
  
  this.indexMon--;

  if (this.indexMon<0) {
    this.indexMon = this.sotrangs[this.sotrangs.length-1];
    

  }
  const start:number = this.sotrangs[this.indexMon]*12;
  const end:number = (this.sotrangs[this.indexMon]+1)*12;
 
  this.mon_sub = this.hanghoas.slice(start,end);
}

//load dữ liệu 
loadData(): any {
  this.service.getAllhanghoa()
  .pipe(map((data: Response)=>{
    return data.json() as HangHoa[];
  })).toPromise().then(x=>{
    this.hanghoas = x;
    this.service.listhanghoa= this.hanghoas;
    let res = [];
    for (let i = 0; i < this.hanghoas.length/12; i++) {
       res.push(i);
    }
   
    this.sotrangs = res;
    this.mon_sub = this.hanghoas.slice(0,12);
  })    
}

loadMon(value){
  this.service.getMonTheoNhom(value)
  .pipe(map((data: Response)=>{
    return data.json() as HangHoa[];
  })).toPromise().then(x=>{
    this.hanghoas = x;
    this.service.listhanghoa= this.hanghoas;
    let res = [];
    for (let i = 0; i < this.hanghoas.length/12; i++) {
       res.push(i);
    }
   
    this.sotrangs = res;
    this.mon_sub = this.hanghoas.slice(0,12);
  })  
}
hanghoa:HangHoa
setTenMon(mon){
  this.service.getMonTheoTen(mon).pipe(map((data: Response)=>{
    return data.json() as HangHoa;
  })).toPromise().then(x=>{
    this.bancashService.getTenMon(x);
  })  
}
chitiethoadon:ChiTietHoaDonReq={
  chitiethoadonID:null,
  ghichu:null,
  giamgia:0,
  hanghoaID:1,
  hoadonID:1,
  soluong:1
}
addDetail(mon:HangHoa){
  delete this.chitiethoadon.chitiethoadonID;
  this.chitiethoadon.hanghoaID=mon.hanghoaID;
  // this.chitiethoadon.soluong=soluong;
  this.OrderService.getOrderFromTableId(this.chitietService.table.banID).subscribe((data:number)=>{
      this.chitiethoadon.hoadonID=data;
      this.chitietService.add(this.chitiethoadon).subscribe(()=>{
        this.chitietService.getChiTietTheoBan(this.chitietService.table.banID).subscribe((data:ChiTietHoaDon[])=>{
          this.chitietService.cts=data
          this.chitietService.soluong=0
          this.chitietService.tongtien = 0
          this.chitietService.cts.forEach(x => {
            this.chitietService.soluong+=x.soluong
            this.chitietService.tongtien+=x.soluong*x.hangHoa.giaban
          });
          alert("them thanh cong");
      })
    });
  });
}


//load dữ liệu 
anhs:Anh[];
getanh(){
  this.anhService.getAllAnh().subscribe((data:Anh[])=>{
   this.anhs=data;
   console.log(this.anhs)
  }
);}
   
      
}
