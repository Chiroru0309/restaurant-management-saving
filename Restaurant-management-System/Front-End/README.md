
## Required

``` bash
node >= 8.9.4
npm > 5.6.0
ng >= 6.1.3  
```
## Installation

``` bash
# clone project
$ git clone http://app.saigontech.edu.vn:6868/ISC-QT06/Restaurant-management-System.git

# di chuyển vào trong app's directory
$ cd Restaurant-management-System

# install app's dependencies
$ npm install
```

## Usage

``` bash
# serve with hot reload at localhost:4200.
$ ng serve --o
# serve with hot reload at localhost:*.
$ ng serve --o --port ***
# build for production with minification
$ ng build
```