USE [master]
GO
/****** Object:  Database [restaurant]    Script Date: 9/21/2018 11:20:01 AM ******/
CREATE DATABASE [restaurant]
 go
USE [restaurant]
GO
/****** Object:  Table [dbo].[ANH]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ANH](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[hanghoa_id] [int] NULL,
	[link] [varchar](200) NULL,
	[alt] [nvarchar](150) NULL,
 CONSTRAINT [PK_ANH] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BAN]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BAN](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tenban] [nvarchar](10) NULL,
	[soghe] [int] NULL,
	[nhomban_id] [int] NULL,
	[trangthai] [bit] NULL,
	[ghichu] [nvarchar](MAX) NULL,
 CONSTRAINT [PK_BAN] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CHITIET_HOADON]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CHITIET_HOADON](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[hanghoa_id] [int] NOT NULL,
	[hoadon_id] [int] NOT NULL,
	[ban_id] [int] NOT NULL,
	[soluong] [int] NULL,
	[ghichu] [nvarchar](MAX) NULL,
	[giamgia] [float] NULL,
 CONSTRAINT [PK_CHITIET_HOADON] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DVT]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DVT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ten_dvt] [nvarchar](10) NULL,
	[hanghoa_id] [int] NULL,
	[giatriquydoi] [int] NULL,
	[giaban] [money] NULL,
	[donvicoban] [nvarchar](50) NULL,
 CONSTRAINT [PK_DVT] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HANG_HOA]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HANG_HOA](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mahanghoa] [varchar](20) NOT NULL,
	[tenhang] [nvarchar](150) NULL,
	[thucdon_id] [int] NOT NULL,
	[nhomhang_id] [int] NOT NULL,
	[loaihang_id] [int] NOT NULL,
	[giavon] [money] NULL,
	[giaban] [money] NULL,
	[tonkho] [int] NULL,
	[dathang] [int] NULL,
	[dmin] [int] NULL,
	[dmnn] [int] NULL,
	[mota] [nvarchar](MAX) NULL,
	[ghichu] [nvarchar](MAX) NULL,
	[lamonthem] [bit] NULL,
	[hinhthucban] [bit] NULL,
	[trangthai] [bit] NULL,
 CONSTRAINT [PK_HANG_HOA] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HOA_DON]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HOA_DON](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ngaylap] [date] NULL,
	[giamgia] [float] NULL,
	[phuongthucthanhtoan_id] [int] NULL,
	[tiendatra] [money] NULL,
	[tinhtrang] [bit] NULL,
 CONSTRAINT [PK_HOA_DON] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LOAIHANG]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LOAIHANG](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ten] [nvarchar](100) NULL,
 CONSTRAINT [PK_LOAIHANG] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MON_AN]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MON_AN](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nguyenlieu_id] [int] NOT NULL,
	[monan_id] [int] NOT NULL,
	[soluong] [int] NULL,
 CONSTRAINT [PK_MON_AN_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MONTHEM]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MONTHEM](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[monan_id] [int] NOT NULL,
	[monthem_id] [int] NOT NULL,
	[soluong] [int] NULL,
 CONSTRAINT [PK_MONTHEM_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NHOMHANG]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NHOMHANG](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ten] [nvarchar](100) NULL,
 CONSTRAINT [PK_NHOMHANG] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PHUONGTHUC_THANHTOAN]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PHUONGTHUC_THANHTOAN](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tenphungthuc] [nvarchar](100) NULL,
 CONSTRAINT [PK_PHUONGTHUC_THANHTOAN] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[THUCDON]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[THUCDON](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ten] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_THUCDON] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[THUOCTINH]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[THUOCTINH](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[hanghoa_id] [int] NOT NULL,
	[ten] [nvarchar](50) NULL,
	[giatri] [nvarchar](100) NULL,
 CONSTRAINT [PK_THUOCTINH_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[BAN] ON 

INSERT [dbo].[BAN] ([id], [tenban], [soghe], [nhomban_id], [trangthai], [ghichu]) VALUES (1, N'Vip', 4, 0, 1, N'Danh cho khach hang vip')
INSERT [dbo].[BAN] ([id], [tenban], [soghe], [nhomban_id], [trangthai], [ghichu]) VALUES (2, N'Thuong', 4, 0, 1, N'Danh cho cac loai khach hang con lai')
INSERT [dbo].[BAN] ([id], [tenban], [soghe], [nhomban_id], [trangthai], [ghichu]) VALUES (3, N'Ban 1', 4, 1, 1, N'Nam o san thuong')
INSERT [dbo].[BAN] ([id], [tenban], [soghe], [nhomban_id], [trangthai], [ghichu]) VALUES (4, N'Ban 2', 4, 2, 1, N'Nam o Tang tret')
INSERT [dbo].[BAN] ([id], [tenban], [soghe], [nhomban_id], [trangthai], [ghichu]) VALUES (5, N'Ban 3', 5, 2, 1, N'Can son lai')
INSERT [dbo].[BAN] ([id], [tenban], [soghe], [nhomban_id], [trangthai], [ghichu]) VALUES (6, N'Ban 4', 2, 1, 1, N'Gan cua so')
INSERT [dbo].[BAN] ([id], [tenban], [soghe], [nhomban_id], [trangthai], [ghichu]) VALUES (7, N'Ban 5', 4, 2, 1, N'Co hoa van rong')
INSERT [dbo].[BAN] ([id], [tenban], [soghe], [nhomban_id], [trangthai], [ghichu]) VALUES (8, N'Ban 6', 4, 1, 1, N'Co cai ghe sat')
INSERT [dbo].[BAN] ([id], [tenban], [soghe], [nhomban_id], [trangthai], [ghichu]) VALUES (9, N'Ban 7', 4, 2, 1, N'Nam o tang tret')
SET IDENTITY_INSERT [dbo].[BAN] OFF
SET IDENTITY_INSERT [dbo].[HANG_HOA] ON 

INSERT [dbo].[HANG_HOA] ([id], [mahanghoa], [tenhang], [thucdon_id], [nhomhang_id], [loaihang_id], [giavon], [giaban], [tonkho], [dathang], [dmin], [dmnn], [mota], [ghichu], [lamonthem], [hinhthucban], [trangthai]) VALUES (1, N'SP00001', N'Rau muong', 1, 1, 1, 5000.0000, 200.0000, 0, 0, 0, 0, NULL, NULL, 0, 1, 1)
INSERT [dbo].[HANG_HOA] ([id], [mahanghoa], [tenhang], [thucdon_id], [nhomhang_id], [loaihang_id], [giavon], [giaban], [tonkho], [dathang], [dmin], [dmnn], [mota], [ghichu], [lamonthem], [hinhthucban], [trangthai]) VALUES (2, N'SP00002', N'Gia', 2, 1, 1, 6000.0000, 400.0000, 0, 0, 0, 0, NULL, NULL, 0, 1, 1)
INSERT [dbo].[HANG_HOA] ([id], [mahanghoa], [tenhang], [thucdon_id], [nhomhang_id], [loaihang_id], [giavon], [giaban], [tonkho], [dathang], [dmin], [dmnn], [mota], [ghichu], [lamonthem], [hinhthucban], [trangthai]) VALUES (3, N'SP00003', N'Toi', 1, 2, 1, 7000.0000, 300.0000, 0, 0, 0, 0, NULL, NULL, 0, 1, 1)
INSERT [dbo].[HANG_HOA] ([id], [mahanghoa], [tenhang], [thucdon_id], [nhomhang_id], [loaihang_id], [giavon], [giaban], [tonkho], [dathang], [dmin], [dmnn], [mota], [ghichu], [lamonthem], [hinhthucban], [trangthai]) VALUES (6, N'SP00004', N'Ot', 1, 2, 1, 8000.0000, 500.0000, 0, 0, 0, 0, NULL, NULL, 0, 1, 1)
SET IDENTITY_INSERT [dbo].[HANG_HOA] OFF
SET IDENTITY_INSERT [dbo].[LOAIHANG] ON 

INSERT [dbo].[LOAIHANG] ([id], [ten]) VALUES (1, N'Nguyên liệu')
INSERT [dbo].[LOAIHANG] ([id], [ten]) VALUES (2, N'ón Ăn')
SET IDENTITY_INSERT [dbo].[LOAIHANG] OFF
SET IDENTITY_INSERT [dbo].[MON_AN] ON 

INSERT [dbo].[MON_AN] ([id], [nguyenlieu_id], [monan_id], [soluong]) VALUES (1, 1, 3, 10)
INSERT [dbo].[MON_AN] ([id], [nguyenlieu_id], [monan_id], [soluong]) VALUES (2, 1, 2, 5)
INSERT [dbo].[MON_AN] ([id], [nguyenlieu_id], [monan_id], [soluong]) VALUES (3, 2, 3, 10)
SET IDENTITY_INSERT [dbo].[MON_AN] OFF
SET IDENTITY_INSERT [dbo].[NHOMHANG] ON 

INSERT [dbo].[NHOMHANG] ([id], [ten]) VALUES (1, N'Trà')
INSERT [dbo].[NHOMHANG] ([id], [ten]) VALUES (2, N'Khai vị')
INSERT [dbo].[NHOMHANG] ([id], [ten]) VALUES (3, N'Lẩu')
SET IDENTITY_INSERT [dbo].[NHOMHANG] OFF
SET IDENTITY_INSERT [dbo].[THUCDON] ON 

INSERT [dbo].[THUCDON] ([id], [ten]) VALUES (1, N'Đồ uống')
INSERT [dbo].[THUCDON] ([id], [ten]) VALUES (2, N'Đồ uống')
INSERT [dbo].[THUCDON] ([id], [ten]) VALUES (3, N'Khác')
SET IDENTITY_INSERT [dbo].[THUCDON] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Table [dbo].[PHIEU_NHAP]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PHIEU_NHAP](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[maphieunhap] [varchar](20) NOT NULL,
	[thoigian] [date] NULL,
	[nhacungcap] [nvarchar](350) NULL,
	[ghichu] [nvarchar](MAX) NULL,
	[giamgia] [decimal](18,4) NULL,
	[datra] [money] NULL,
	[trangthai] [bit] NULL,
 CONSTRAINT [PK_PHIEU_NHAP] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CHITIET_PHIEUNHAP]    Script Date: 9/21/2018 11:20:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CHITIET_PHIEUNHAP](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[phieunhap_id] [int] NULL,
	[hanghoa_id] [int] NULL,
	[soluong] [int] NULL,
	[dongia] [money] NULL,
	[giamgia] [decimal](18,4) NULL,
 CONSTRAINT [PK_CHITIET_PHIEUNHAP] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


SET IDENTITY_INSERT [dbo].[PHIEU_NHAP] ON 
INSERT [dbo].[PHIEU_NHAP] ([id], [maphieunhap], [thoigian], [nhacungcap], [giamgia], [datra], [trangthai], [ghichu]) VALUES (1, N'PN00001', '2018/09/18', N'Coopmart', 0, 0, 1, N'Mua ở siêu thị')
INSERT [dbo].[PHIEU_NHAP] ([id], [maphieunhap], [thoigian], [nhacungcap], [giamgia], [datra], [trangthai], [ghichu]) VALUES (2, N'PN00002', '2017/08/23', N'Chợ Lớn', 10, 0, 0, N'Mua ở chợ')
INSERT [dbo].[PHIEU_NHAP] ([id], [maphieunhap], [thoigian], [nhacungcap], [giamgia], [datra], [trangthai], [ghichu]) VALUES (3, N'PN00003', '2017/08/23', N'Chợ Lớn', 10, 80000, 1, N'Mua ở chợ')
SET IDENTITY_INSERT [dbo].[PHIEU_NHAP] OFF

SET IDENTITY_INSERT [dbo].[CHITIET_PHIEUNHAP] ON 
INSERT [dbo].[CHITIET_PHIEUNHAP] ([id], [phieunhap_id], [hanghoa_id], [soluong], [dongia], [giamgia]) VALUES (1, 1, 1, 2, 6500, 0)
INSERT [dbo].[CHITIET_PHIEUNHAP] ([id], [phieunhap_id], [hanghoa_id], [soluong], [dongia], [giamgia]) VALUES (2, 1, 2, 5, 4000, 0)
INSERT [dbo].[CHITIET_PHIEUNHAP] ([id], [phieunhap_id], [hanghoa_id], [soluong], [dongia], [giamgia]) VALUES (3, 2, 1, 10, 7000, 5)
INSERT [dbo].[CHITIET_PHIEUNHAP] ([id], [phieunhap_id], [hanghoa_id], [soluong], [dongia], [giamgia]) VALUES (4, 2, 2, 5, 4000, 0)
INSERT [dbo].[CHITIET_PHIEUNHAP] ([id], [phieunhap_id], [hanghoa_id], [soluong], [dongia], [giamgia]) VALUES (5, 3, 1, 10, 7000, 5)
INSERT [dbo].[CHITIET_PHIEUNHAP] ([id], [phieunhap_id], [hanghoa_id], [soluong], [dongia], [giamgia]) VALUES (6, 3, 2, 5, 4000, 0)
INSERT [dbo].[CHITIET_PHIEUNHAP] ([id], [phieunhap_id], [hanghoa_id], [soluong], [dongia], [giamgia]) VALUES (7, 3, 6, 1, 15000, 0)
SET IDENTITY_INSERT [dbo].[CHITIET_PHIEUNHAP] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [UniqueMahanghoa]    Script Date: 9/21/2018 11:20:01 AM ******/
ALTER TABLE [dbo].[HANG_HOA] ADD  CONSTRAINT [UniqueMahanghoa] UNIQUE NONCLUSTERED 
(
	[mahanghoa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CHITIET_HOADON] ADD  CONSTRAINT [DF_CHITIET_HOADON_soluong]  DEFAULT ((0)) FOR [soluong]
GO
ALTER TABLE [dbo].[HANG_HOA] ADD  CONSTRAINT [DF_HANG_HOA_giavon]  DEFAULT ((0)) FOR [giavon]
GO
ALTER TABLE [dbo].[HANG_HOA] ADD  CONSTRAINT [DF_HANG_HOA_giaban]  DEFAULT ((0)) FOR [giaban]
GO
ALTER TABLE [dbo].[HANG_HOA] ADD  CONSTRAINT [DF_HANG_HOA_tonkho]  DEFAULT ((0)) FOR [tonkho]
GO
ALTER TABLE [dbo].[HANG_HOA] ADD  CONSTRAINT [DF_HANG_HOA_dathang]  DEFAULT ((0)) FOR [dathang]
GO
ALTER TABLE [dbo].[HANG_HOA] ADD  CONSTRAINT [DF_HANG_HOA_dmin]  DEFAULT ((0)) FOR [dmin]
GO
ALTER TABLE [dbo].[HANG_HOA] ADD  CONSTRAINT [DF_HANG_HOA_dmnn]  DEFAULT ((0)) FOR [dmnn]
GO
ALTER TABLE [dbo].[HANG_HOA] ADD  CONSTRAINT [DF_HANG_HOA_lamonthem]  DEFAULT ((0)) FOR [lamonthem]
GO
ALTER TABLE [dbo].[HANG_HOA] ADD  CONSTRAINT [DF_HANG_HOA_hinhthucban]  DEFAULT ((1)) FOR [hinhthucban]
GO
ALTER TABLE [dbo].[HANG_HOA] ADD  CONSTRAINT [DF_HANG_HOA_trangthai]  DEFAULT ((1)) FOR [trangthai]
GO
ALTER TABLE [dbo].[HOA_DON] ADD  CONSTRAINT [DF_HOA_DON_giamgia]  DEFAULT ((0)) FOR [giamgia]
GO
ALTER TABLE [dbo].[HOA_DON] ADD  CONSTRAINT [DF_HOA_DON_tinhtrang]  DEFAULT ((1)) FOR [tinhtrang]
GO
ALTER TABLE [dbo].[MON_AN] ADD  CONSTRAINT [DF_MON_AN_soluong]  DEFAULT ((0)) FOR [soluong]
GO
ALTER TABLE [dbo].[ANH]  WITH CHECK ADD  CONSTRAINT [FK_ANH_HANG_HOA] FOREIGN KEY([hanghoa_id])
REFERENCES [dbo].[HANG_HOA] ([id])
GO
ALTER TABLE [dbo].[ANH] CHECK CONSTRAINT [FK_ANH_HANG_HOA]
GO
ALTER TABLE [dbo].[CHITIET_HOADON]  WITH CHECK ADD  CONSTRAINT [FK_CHITIET_HOADON_BAN] FOREIGN KEY([ban_id])
REFERENCES [dbo].[BAN] ([id])
GO
ALTER TABLE [dbo].[CHITIET_HOADON] CHECK CONSTRAINT [FK_CHITIET_HOADON_BAN]
GO
ALTER TABLE [dbo].[CHITIET_HOADON]  WITH CHECK ADD  CONSTRAINT [FK_CHITIET_HOADON_HANG_HOA] FOREIGN KEY([hanghoa_id])
REFERENCES [dbo].[HANG_HOA] ([id])
GO
ALTER TABLE [dbo].[CHITIET_HOADON] CHECK CONSTRAINT [FK_CHITIET_HOADON_HANG_HOA]
GO
ALTER TABLE [dbo].[CHITIET_HOADON]  WITH CHECK ADD  CONSTRAINT [FK_CHITIET_HOADON_HOA_DON] FOREIGN KEY([hoadon_id])
REFERENCES [dbo].[HOA_DON] ([id])
GO
ALTER TABLE [dbo].[CHITIET_HOADON] CHECK CONSTRAINT [FK_CHITIET_HOADON_HOA_DON]
GO
ALTER TABLE [dbo].[HANG_HOA]  WITH CHECK ADD  CONSTRAINT [FK_HANG_HOA_LOAIHANG] FOREIGN KEY([loaihang_id])
REFERENCES [dbo].[LOAIHANG] ([id])
GO
ALTER TABLE [dbo].[HANG_HOA] CHECK CONSTRAINT [FK_HANG_HOA_LOAIHANG]
GO
ALTER TABLE [dbo].[HANG_HOA]  WITH CHECK ADD  CONSTRAINT [FK_HANG_HOA_NHOMHANG] FOREIGN KEY([nhomhang_id])
REFERENCES [dbo].[NHOMHANG] ([id])
GO
ALTER TABLE [dbo].[HANG_HOA] CHECK CONSTRAINT [FK_HANG_HOA_NHOMHANG]
GO
ALTER TABLE [dbo].[HANG_HOA]  WITH CHECK ADD  CONSTRAINT [FK_HANG_HOA_THUCDON] FOREIGN KEY([thucdon_id])
REFERENCES [dbo].[THUCDON] ([id])
GO
ALTER TABLE [dbo].[HANG_HOA] CHECK CONSTRAINT [FK_HANG_HOA_THUCDON]
GO
ALTER TABLE [dbo].[HOA_DON]  WITH CHECK ADD  CONSTRAINT [FK_HOA_DON_PHUONGTHUC_THANHTOAN] FOREIGN KEY([phuongthucthanhtoan_id])
REFERENCES [dbo].[PHUONGTHUC_THANHTOAN] ([id])
GO
ALTER TABLE [dbo].[HOA_DON] CHECK CONSTRAINT [FK_HOA_DON_PHUONGTHUC_THANHTOAN]
GO
ALTER TABLE [dbo].[MON_AN]  WITH CHECK ADD  CONSTRAINT [FK_MON_AN_HANG_HOA] FOREIGN KEY([monan_id])
REFERENCES [dbo].[HANG_HOA] ([id])
GO
ALTER TABLE [dbo].[MON_AN] CHECK CONSTRAINT [FK_MON_AN_HANG_HOA]
GO
ALTER TABLE [dbo].[MON_AN]  WITH CHECK ADD  CONSTRAINT [FK_MON_AN_HANG_HOA1] FOREIGN KEY([nguyenlieu_id])
REFERENCES [dbo].[HANG_HOA] ([id])
GO
ALTER TABLE [dbo].[MON_AN] CHECK CONSTRAINT [FK_MON_AN_HANG_HOA1]
GO
ALTER TABLE [dbo].[MONTHEM]  WITH CHECK ADD  CONSTRAINT [FK_MONTHEM_HANG_HOA] FOREIGN KEY([monan_id])
REFERENCES [dbo].[HANG_HOA] ([id])
GO
ALTER TABLE [dbo].[MONTHEM] CHECK CONSTRAINT [FK_MONTHEM_HANG_HOA]
GO
ALTER TABLE [dbo].[MONTHEM]  WITH CHECK ADD  CONSTRAINT [FK_MONTHEM_HANG_HOA1] FOREIGN KEY([monthem_id])
REFERENCES [dbo].[HANG_HOA] ([id])
GO
ALTER TABLE [dbo].[MONTHEM] CHECK CONSTRAINT [FK_MONTHEM_HANG_HOA1]
GO
ALTER TABLE [dbo].[THUOCTINH]  WITH CHECK ADD  CONSTRAINT [FK_THUOCTINH_HANG_HOA] FOREIGN KEY([hanghoa_id])
REFERENCES [dbo].[HANG_HOA] ([id])
GO
ALTER TABLE [dbo].[THUOCTINH] CHECK CONSTRAINT [FK_THUOCTINH_HANG_HOA]
/****** Object:  Index [UniqueMaphieunhap]    Script Date: 9/21/2018 11:20:01 AM ******/
ALTER TABLE [dbo].[PHIEU_NHAP] ADD  CONSTRAINT [UniqueMaphieunhap] UNIQUE NONCLUSTERED 
(
	[maphieunhap] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CHITIET_PHIEUNHAP] ADD  CONSTRAINT [DF_CHITIET_PHIEUNHAP_soluong]  DEFAULT ((0)) FOR [soluong]
GO
ALTER TABLE [dbo].[CHITIET_PHIEUNHAP] ADD  CONSTRAINT [DF_CHITIET_PHIEUNHAP_dongia]  DEFAULT ((0)) FOR [dongia]
GO
ALTER TABLE [dbo].[CHITIET_PHIEUNHAP] ADD  CONSTRAINT [DF_CHITIET_PHIEUNHAP_giamgia]  DEFAULT ((0)) FOR [giamgia]
GO
ALTER TABLE [dbo].[PHIEU_NHAP] ADD  CONSTRAINT [DF_PHIEU_NHAP_thoigian]  DEFAULT ((GETDATE())) FOR [thoigian]
GO
ALTER TABLE [dbo].[PHIEU_NHAP] ADD  CONSTRAINT [DF_PHIEU_NHAP_giamgia]  DEFAULT ((0)) FOR [giamgia]
GO
ALTER TABLE [dbo].[PHIEU_NHAP] ADD  CONSTRAINT [DF_PHIEU_NHAP_datra]  DEFAULT ((0)) FOR [datra]
GO
ALTER TABLE [dbo].[PHIEU_NHAP] ADD  CONSTRAINT [DF_PHIEU_NHAP_trangthai]  DEFAULT ((1)) FOR [trangthai]
GO
ALTER TABLE [dbo].[CHITIET_PHIEUNHAP]  WITH CHECK ADD  CONSTRAINT [FK_CHITIET_PHIEUNHAP_PHIEU_NHAP] FOREIGN KEY([phieunhap_id])
REFERENCES [dbo].[PHIEU_NHAP] ([id])
GO
ALTER TABLE [dbo].[CHITIET_PHIEUNHAP] CHECK CONSTRAINT [FK_CHITIET_PHIEUNHAP_PHIEU_NHAP]
GO
ALTER TABLE [dbo].[CHITIET_PHIEUNHAP]  WITH CHECK ADD  CONSTRAINT [FK_CHITIET_PHIEUNHAP_HANG_HOA] FOREIGN KEY([hanghoa_id])
REFERENCES [dbo].[HANG_HOA] ([id])
GO
ALTER TABLE [dbo].[CHITIET_PHIEUNHAP] CHECK CONSTRAINT [FK_CHITIET_PHIEUNHAP_HANG_HOA]
GO
USE [master]
GO
ALTER DATABASE [restaurant] SET  READ_WRITE 
GO
