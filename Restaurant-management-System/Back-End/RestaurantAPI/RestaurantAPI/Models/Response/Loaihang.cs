﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestaurantAPI.Models.Response
{
    [Table("LOAIHANG")]
    public class Loaihang
    {
        [Key]
        [Column("id")]
        public int loaihangID { get; set; }
        public string ten { get; set; }
    }
}