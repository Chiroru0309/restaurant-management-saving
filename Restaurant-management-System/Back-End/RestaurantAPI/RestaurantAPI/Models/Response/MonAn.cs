﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    [Table("MON_AN")]
    public class MonAn
    {
        [Key]
        public int id { get; set; }

        [ForeignKey("nguyenlieu_id")]
        public virtual HangHoa NguyenLieu { get; set; }

        
        [ForeignKey("monan_id")]
        public virtual HangHoa MonCheBien { get; set; }

        public int soluong { get; set; }

     
    }
}
