﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    [Table("HANG_HOA")]
    public class HangHoa
    {
        [Key]
        [Column("id")]
        public int hanghoaID { get; set; }
        public string mahanghoa { get; set; }
        public string tenhang { get; set; }
        public decimal giavon { get; set; }
        public decimal giaban { get; set; }
        public int tonkho { get; set; }
        public int dathang { get; set; }
        public int dmin { get; set; }
        public int dmnn { get; set; }
        public string mota { get; set; }
        public string ghichu { get; set; }
        public bool lamonthem { get; set; }
        public bool hinhthucban { get; set; }
        public bool trangthai { get; set; }


        [ForeignKey("thucdon_id")]
        public virtual ThucDon ThucDon { get; set; }


        [ForeignKey("nhomhang_id")]
        public virtual NhomHang NhomHang { get; set; }


        [ForeignKey("loaihang_id")]
        public virtual Loaihang Loaihang { get; set; }
    }
}
