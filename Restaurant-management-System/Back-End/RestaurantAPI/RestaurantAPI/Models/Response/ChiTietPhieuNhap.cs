﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    [Table("CHITIET_PHIEUNHAP")]
    public class ChiTietPhieuNhap
    {
        [Key]
        [Column("id")]
        public int chitietphieunhapID { get; set; }
        public int soluong { get; set; }
        public decimal dongia { get; set; }
        public decimal giamgia { get; set; }
        [ForeignKey("phieunhap_id")]
        public PhieuNhap PhieuNhap { get; set; }
        [ForeignKey("hanghoa_id")]
        public HangHoa HangHoa { get; set; }
    }
}
