﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    [Table("THUOCTINH")]
    public class ThuocTinh
    {
        [Key]
        [Column("id")]
        public int thuoctinhID { get; set; }
        [Column("ten")]
        public string ten { get; set; }
        [Column("giatri")]
        public string giatri { get; set; }
        [ForeignKey("hanghoa_id")]
        public virtual HangHoa HangHoa { get; set; }
    }
}
