﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace RestaurantAPI.Models.Response
{
    [Table("ANH")]
    public class HinhAnh
    {
        [Key]
        [Column("id")]
        public int AnhID { get; set; }

        [Column("hanghoa_id")]
        public int hanghoaID { get; set; }
        [ForeignKey("hanghoaID")]
        public virtual HangHoa HangHoa { get; set; }

        public string link { get; set; }

        public string alt { get; set; }

    }
}
