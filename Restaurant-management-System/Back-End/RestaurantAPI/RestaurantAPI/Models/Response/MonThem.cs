﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    [Table("MONTHEM")]
    public class MonThem
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [ForeignKey("monan_id")]
        public virtual HangHoa MonAn { get; set; }

        [ForeignKey("monthem_id")]
        public virtual HangHoa Monthem { get; set; }

        public int soluong { get; set; }
    }
}
