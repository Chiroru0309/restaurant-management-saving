﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    [Table("PHIEU_NHAP")]
    public class PhieuNhap
    {
        [Key]
        public int id { get; set; }
        public string maphieunhap { get; set; }
        public DateTime thoigian { get; set; }
        public string nhacungcap { get; set; }
        public string ghichu { get; set; }
        public decimal giamgia { get; set; }
        public decimal datra { get; set; }
        public bool trangthai { get; set; }
    }
}
