﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    [Table("HOA_DON")]
    public class HoaDon
    {
        [Key]
        [Column("id")]
        public int hoadonID { get; set; }
        public DateTime? ngaylap { get; set; }
        public double giamgia { get; set; }
        [ForeignKey("phuongthucthanhtoan_id")]
        public virtual PhuongThucThanhToan PhuongThucThanhToan { get; set; }
        [ForeignKey("ban_id")]
        public virtual Ban Ban { get; set; }
        public decimal tiendatra { get; set; }
        public bool tinhtrang { get; set; }
    }
}
