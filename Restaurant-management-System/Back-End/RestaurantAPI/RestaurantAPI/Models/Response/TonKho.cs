﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    public class TonKho
    {
        public string mahang { get; set; }
        public string tenhang { get; set; }
        public int tondau { get; set; }
        public int nhap { get; set; }
        public int xuat { get; set; }
        public int toncuoi { get; set; }
    }
}
