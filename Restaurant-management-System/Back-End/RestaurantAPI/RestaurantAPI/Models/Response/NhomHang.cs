﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestaurantAPI.Models.Response
{
    [Table("NHOMHANG")]
    public class NhomHang
    {
        [Key]
        [Column("id")]
        public int nhomhangID { get; set; }
        public string ten { get; set; }
    }
}
