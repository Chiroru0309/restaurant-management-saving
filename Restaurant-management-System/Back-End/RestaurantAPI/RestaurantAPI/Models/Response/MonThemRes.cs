﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    public class MonThemRes
    {
        [Key]
        public int monthemID { get; set; }

        public string tenmonan { get; set; }
        public string tenmonthem { get; set; }
        public decimal giabanmonan { get; set; }
        public decimal giabanmonthem { get; set; }
        public int soluong { get; set; }
    }
}
