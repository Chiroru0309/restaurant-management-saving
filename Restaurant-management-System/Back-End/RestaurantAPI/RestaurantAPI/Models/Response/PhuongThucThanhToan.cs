﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    [Table("PHUONGTHUC_THANHTOAN")]
    public class PhuongThucThanhToan
    {
        [Key]
        [Column("id")]
        public int ptttId { get; set; }
        [Column("tenphuongthuc")]
        public string tenphuongthuc { get; set; }
    }
}
