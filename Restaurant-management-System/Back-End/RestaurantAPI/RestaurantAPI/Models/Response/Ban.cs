﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    [Table("BAN")]
    public class Ban
    {
        [Key]
        [Column("id")]
        public int banID { get; set; }
        public string tenban { get; set; }
        public int soghe { get; set; }

        [Column("nhomban_id")]
        public int nhombanID { get; set; }
        public string ghichu { get; set; }
        public bool trangthai { get; set; }

    }
}
