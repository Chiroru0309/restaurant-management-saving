﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    [Table("DVT")]
    public class DVT
    {
        [Key]
        [Column("id")]
        public int dvtID { get; set; }
        [Column("ten_dvt")]
        public string ten { get; set; }
        [ForeignKey("hanghoa_id")]
        public HangHoa Hanghoa { get; set; }

        public int giatriquydoi { get; set; }
        public decimal giaban { get; set; }

        public string donvicoban { get; set; }
    }
}
