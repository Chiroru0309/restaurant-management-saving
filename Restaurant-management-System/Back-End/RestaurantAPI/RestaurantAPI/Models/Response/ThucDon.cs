﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    [Table("THUCDON")]
    public class ThucDon
    {
        [Key]
        [Column("id")]
        public int thucdonID { get; set; }
        public string ten { get; set; }

    }
}
