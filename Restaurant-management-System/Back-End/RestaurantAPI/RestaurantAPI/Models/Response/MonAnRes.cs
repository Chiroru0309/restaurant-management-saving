﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    public class MonAnRes
    {
        [Key]
        public int monchebienID { get; set; }

        public string tenmonan { get; set; }
        public string tennguyenlieu{ get; set; }
        public decimal giabanmonan { get; set; }
        public decimal giabannguyenlieu { get; set; }
        public int soluong { get; set; }
    }
}
