﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Response
{
    [Table("CHITIET_HOADON")]
    public class ChiTietHoaDon
    {
        [Key]
        [Column("id")]
        public int chitiethoadonID { get; set; }
        [ForeignKey("hanghoa_id")]
        public virtual HangHoa HangHoa { get; set; }
        [ForeignKey("hoadon_id")]
        public virtual HoaDon HoaDon { get; set; }
        public int soluong { get; set; }
        public string ghichu { get; set; }
        public double giamgia { get; set; }
    }
}
