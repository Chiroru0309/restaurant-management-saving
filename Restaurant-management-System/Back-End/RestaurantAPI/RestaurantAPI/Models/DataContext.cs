﻿using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models.Request;
using RestaurantAPI.Models.Response;
using RestaurantAPI.Models.Resquest;

namespace RestaurantAPI.Models
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }


        public DbSet<NhomHang> DBNhomHangs { get; set; }
        public DbSet<MonAn> DBMonAns { get; set; }
        public DbSet<HangHoa> HangHoas { get; set; }
        public DbSet<HangHoaReq> HangHoaReq { get; set; }


        public DbSet<MonThem> MonThems { get; set; }
        public DbSet<Loaihang> Loaihangs { get; set; }
        public DbSet<NhomHang> NhomHangs { get; set; }


        public DbSet<Ban> Ban { get; set; }
        public DbSet<ThucDon> ThucDons { get; set; }

        public DbSet<HinhAnh> HinhAnhs { get; set; }
        public DbSet<AnhReq> AnhReqs { get; set; }
        public DbSet<HoaDon> HoaDons { get; set; }

        public DbSet<PhieuNhap> PhieuNhaps { get; set; }
        public DbSet<ChiTietPhieuNhap> ChiTietPhieuNhaps { get; set; }
        public DbSet<ChiTietHoaDon> ChiTietHoaDons { get; set; }
        public DbSet<RestaurantAPI.Models.Response.DVT> DVT { get; set; }
        public DbSet<RestaurantAPI.Models.Response.ThuocTinh> ThuocTinh { get; set; }
		
		public DbSet<RestaurantAPI.Models.Response.HoaDon> HoaDon { get; set; }
        public DbSet<RestaurantAPI.Models.Response.ChiTietHoaDon> ChiTietHoaDon { get; set; }
        public DbSet<RestaurantAPI.Models.Response.PhuongThucThanhToan> PhuongThucThanhToan { get; set; }
    }
}
