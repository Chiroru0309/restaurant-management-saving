﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Request
{
    public class BanReq
    {
        [Key]
        public int banID { get; set; }
        public string tenban { get; set; }
        public int soghe { get; set; }

        public int nhombanID { get; set; }
        public string ghichu { get; set; }
        public bool trangthai { get; set; }
    }
}
