﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Request
{
    public class HoaDonReq
    {
        [Key]
       
        public int hoadonID { get; set; }

        public DateTime? ngaylap { get; set; }
        public double giamgia { get; set; }

        public int banID { get; set; }
        public  int PhuongThucThanhToanId { get; set; }

        public decimal tiendatra { get; set; }

        public bool tinhtrang { get; set; }
    }
}
