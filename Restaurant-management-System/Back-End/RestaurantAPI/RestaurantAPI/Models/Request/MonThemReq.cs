﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Request
{
    public class MonThemReq
    {
        [Key]
        public int monthemreqID { get; set; }
        public int monanID { get; set; }
        public int monthemID { get; set; }
        public int soluong { get; set; }
    }
}
