﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Request
{
    public class AnhReq
    {
            [Key]
            [Column("id")]
            public int AnhID { get; set; }
            [Column("hanghoa_id")]
            public int hanghoaID { get; set; }
            public string link { get; set; }
            public string alt { get; set; }
        }
    
}
