﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Request
{
    public class ThuocTinhReq
    {
        [Key]
        public int thuoctinhid { get; set; }
        public string tenthuoctinh { get; set; }
        public int hanghoaid { get; set; }
        public string giatri { get; set; }
       
    }
}
