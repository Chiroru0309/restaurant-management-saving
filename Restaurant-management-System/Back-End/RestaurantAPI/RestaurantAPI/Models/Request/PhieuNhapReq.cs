﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Resquest
{

    public class PhieuNhapReq
    {
        [Key]
       // public int chitietphieunhapID { get; set; }
        public int phieunhapID { get; set; }
        public string mahanghoa { get; set; }
        public int soluong { get; set; }
        public decimal dongia { get; set; }
        public decimal giamgia { get; set; }
    }
}
