﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Request
{
    public class MonAnReq
    {
        [Key]
        public int moncehbienID { get; set; }
        public int monanID { get; set; }
        public int nguyenlieuID { get; set; }
        public int soluong { get; set; }
    }
}
