﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Request
{
    public class ChiTietHoaDonReq
    {
        [Key]

         public int chitiethoadonID { get; set; }
 
        public  int hanghoaID { get; set; }

        public  int hoadonID { get; set; }



        public int soluong { get; set; }
        public string ghichu { get; set; }
        public double giamgia { get; set; }
    }
}
