﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Request
{
    public class DateRange
    {
        public DateTime startdate { get; set; }
        public DateTime enddate { get; set; }
    }
}
