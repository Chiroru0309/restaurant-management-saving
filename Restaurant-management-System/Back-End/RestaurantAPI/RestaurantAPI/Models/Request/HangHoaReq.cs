﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Models.Resquest
{
   
    public class HangHoaReq
    {
        [Key]
        public int hanghoaID { get; set; }
        public string mahanghoa { get; set; }
        public string tenhang { get; set; }
        public decimal giavon { get; set; }
        public decimal giaban { get; set; }
        public int tonkho { get; set; }
        public int dathang { get; set; }
        public int dmin { get; set; }
        public int dmnn { get; set; }
        public string mota { get; set; }
        public string ghichu { get; set; }
        public bool lamonthem { get; set; }
        public bool hinhthucban { get; set; }
        public bool trangthai { get; set; }
      
        public int thucdonID  { get; set; }
        
        public int nhomhangID  { get; set; }
        
        public int loaihangID  { get; set; }
    }
}
