﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models;
using RestaurantAPI.Models.Request;
using RestaurantAPI.Models.Response;

namespace RestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MonThemsController : ControllerBase
    {
        private readonly DataContext _context;

        public MonThemsController(DataContext context)
        {
            _context = context;
        }


        // GET: api/MonThems/5
        [HttpGet("bymonan/{id}")]
        public IEnumerable<MonThemRes> GetMonThemByMonAn([FromRoute] int id)
        {          
            var monThems = _context.MonThems.Where(x=>x.MonAn.hanghoaID == id).Select(x => new MonThemRes() {
                monthemID = x.id,
                tenmonan = x.MonAn.tenhang,
                tenmonthem = x.Monthem.tenhang,
                giabanmonan= x.MonAn.giaban,
                giabanmonthem = x.Monthem.giaban,
                soluong = x.soluong
            }).ToList();         
            return monThems;
        }

        // GET: api/MonThems/5
        [HttpGet("bymonthem/{id}")]
        public IEnumerable<MonThemRes> GetMonAnByMonThem([FromRoute] int id)
        {
            var monThems = _context.MonThems.Where(x => x.Monthem.hanghoaID == id).Select(x => new MonThemRes()
            {
                monthemID = x.id,
                tenmonan = x.MonAn.tenhang,
                tenmonthem = x.Monthem.tenhang,
                giabanmonan = x.MonAn.giaban,
                giabanmonthem = x.Monthem.giaban,
                soluong = x.soluong
            }).ToList();
            return monThems;
        }

        // POST: api/MonThems
        [HttpPost]
        public IActionResult PostMonThem([FromBody] MonThemReq monThemreq)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var monthem = new MonThem();
            monthem.id = monThemreq.monthemreqID;
            monthem.MonAn = _context.HangHoas.Find(monThemreq.monanID);
            monthem.Monthem = _context.HangHoas.Find(monThemreq.monthemID);
            monthem.soluong = monThemreq.soluong;
            _context.MonThems.Add(monthem);
            _context.SaveChangesAsync();

            return CreatedAtAction("GetMonThem", new { id = monthem.id }, monthem);
        }

        // DELETE: api/MonThems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMonThem([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var monThem = await _context.MonThems.FindAsync(id);
            if (monThem == null)
            {
                return NotFound();
            }

            _context.MonThems.Remove(monThem);
            await _context.SaveChangesAsync();

            return Ok(monThem);
        }

        private bool MonThemExists(int id)
        {
            return _context.MonThems.Any(e => e.id == id);
        }
    }
}