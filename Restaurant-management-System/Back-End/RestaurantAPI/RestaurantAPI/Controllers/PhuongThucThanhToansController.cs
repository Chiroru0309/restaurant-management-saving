﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models;
using RestaurantAPI.Models.Response;

namespace RestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhuongThucThanhToansController : ControllerBase
    {
        private readonly DataContext _context;

        public PhuongThucThanhToansController(DataContext context)
        {
            _context = context;
        }

        // GET: api/PhuongThucThanhToans
        [HttpGet]
        public IEnumerable<PhuongThucThanhToan> GetPhuongThucThanhToan()
        {
            return _context.PhuongThucThanhToan;
        }

        // GET: api/PhuongThucThanhToans/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPhuongThucThanhToan([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var phuongThucThanhToan = await _context.PhuongThucThanhToan.FindAsync(id);

            if (phuongThucThanhToan == null)
            {
                return NotFound();
            }

            return Ok(phuongThucThanhToan);
        }

        // PUT: api/PhuongThucThanhToans/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPhuongThucThanhToan([FromRoute] int id, [FromBody] PhuongThucThanhToan phuongThucThanhToan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != phuongThucThanhToan.ptttId)
            {
                return BadRequest();
            }

            _context.Entry(phuongThucThanhToan).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PhuongThucThanhToanExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PhuongThucThanhToans
        [HttpPost]
        public async Task<IActionResult> PostPhuongThucThanhToan([FromBody] PhuongThucThanhToan phuongThucThanhToan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.PhuongThucThanhToan.Add(phuongThucThanhToan);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPhuongThucThanhToan", new { id = phuongThucThanhToan.ptttId }, phuongThucThanhToan);
        }

        // DELETE: api/PhuongThucThanhToans/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePhuongThucThanhToan([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var phuongThucThanhToan = await _context.PhuongThucThanhToan.FindAsync(id);
            if (phuongThucThanhToan == null)
            {
                return NotFound();
            }

            _context.PhuongThucThanhToan.Remove(phuongThucThanhToan);
            await _context.SaveChangesAsync();

            return Ok(phuongThucThanhToan);
        }

        private bool PhuongThucThanhToanExists(int id)
        {
            return _context.PhuongThucThanhToan.Any(e => e.ptttId == id);
        }
    }
}