﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models;
using RestaurantAPI.Models.Request;
using RestaurantAPI.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowSpecificOrigin")]
    [ApiController]
    public class AnhsController
    {
        private readonly DataContext _context;

        public AnhsController(DataContext context)
        {
            _context = context;
        }

        // GET: api/Anhs
        [HttpGet]
        public IEnumerable<AnhReq> GetAnh()
        {
            return _context.HinhAnhs.Include(x=>x.HangHoa).Select(x=>new AnhReq {
                AnhID=x.AnhID,
                hanghoaID=x.hanghoaID,
                link=x.link,
                alt=x.alt
            }).AsNoTracking().ToList();
        }
    }
}
