﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models;
using RestaurantAPI.Models.Request;
using RestaurantAPI.Models.Response;
using RestaurantAPI.Models.Resquest;

namespace RestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HangHoasController : ControllerBase
    {
        private readonly DataContext _context;

        public HangHoasController(DataContext context)
        {
            _context = context;
        }

        // GET: api/HangHoas
        [HttpGet]
        public IEnumerable<HangHoa> GetHangHoa()
        {
            // The following line loads the contacts
            return _context.HangHoas
                .Include(x=>x.ThucDon)
                .Include(x=>x.NhomHang)
                .Include(x=>x.Loaihang)   
                .ToList();
        }



        // GET: api/HangHoas
        [HttpGet("getmontheonhom/{id}")]
        public IEnumerable<HangHoa> GetMonTheoNhom([FromRoute] int id)
        {
            return _context.HangHoas.Where(x => x.NhomHang.nhomhangID == id).Include(x => x.ThucDon)
                .Include(x => x.NhomHang)
                .Include(x => x.Loaihang) .ToList();
        }
        // GET: api/HangHoas
        [HttpGet("getmontheoten/{id}")]
        public IEnumerable<HangHoa> GetMonTheoTen([FromRoute] string id)
        {
            return _context.HangHoas.Where(x => x.tenhang == id).Include(x => x.ThucDon)
                .Include(x => x.NhomHang)
                .Include(x => x.Loaihang).ToList();
        }
        // GET: api/HangHoas/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetHangHoa([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var hangHoa = await _context.HangHoas.FindAsync(id);

            if (hangHoa == null)
            {
                return NotFound();
            }

            return Ok(hangHoa);

        }
		// GET: api/HangHoas/5
        [HttpGet("GetPriceFromID/{id}")]
        public decimal GetPriceFromID([FromRoute] int id)
        {
            return _context.HangHoas.Where(x => x.hanghoaID == id).FirstOrDefault().giaban;
        }
		
        // PUT: api/HangHoas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutHangHoa([FromRoute] int id, [FromBody] HangHoaReq hangHoa)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hangHoa.hanghoaID)
            {
                return BadRequest();
            }
            var hanghoa = _context.HangHoas.Where(x => x.hanghoaID == hangHoa.hanghoaID).FirstOrDefault();
            if (hanghoa==null)
            {
                return NotFound();
            }
            hanghoa.hanghoaID = hangHoa.hanghoaID;
            hanghoa.mahanghoa = hangHoa.mahanghoa;
            hanghoa.tenhang = hangHoa.tenhang;
            hanghoa.giavon = hangHoa.giavon;
            hanghoa.giaban = hangHoa.giaban;
            hanghoa.tonkho = hangHoa.tonkho;
            hanghoa.dathang = hangHoa.dathang;
            hanghoa.dmin = hangHoa.dmin;
            hanghoa.dmnn = hangHoa.dmnn;
            //hanghoa.ghichu = hangHoa.ghichu;
            hanghoa.lamonthem = hangHoa.lamonthem;
            hanghoa.hinhthucban = hangHoa.hinhthucban;
            hanghoa.trangthai = hangHoa.trangthai;
            hanghoa.ThucDon =_context.ThucDons.Find(hangHoa.thucdonID);
            hanghoa.Loaihang = _context.Loaihangs.Find(hangHoa.loaihangID);
            hanghoa.NhomHang = _context.NhomHangs.Find(hangHoa.nhomhangID);
            hanghoa.mota = hangHoa.mota;
            _context.Entry(hanghoa).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HangHoaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetHangHoa", new { id = hanghoa.hanghoaID }, hanghoa);
        }

        // POST: api/HangHoas
        [HttpPost]
        public async Task<IActionResult> PostHangHoa([FromBody] HangHoaReq hangHoa)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            HangHoa hanghoa = new HangHoa();
            hanghoa.mahanghoa = hangHoa.mahanghoa;
            hanghoa.tenhang = hangHoa.tenhang;
            hanghoa.giavon = hangHoa.giavon;
            hanghoa.giaban = hangHoa.giaban;
            hanghoa.tonkho = hangHoa.tonkho;
            hanghoa.dathang = hangHoa.dathang;
            hanghoa.dmin = hangHoa.dmin;
            hanghoa.dmnn = hangHoa.dmnn;
            hanghoa.ghichu = hangHoa.ghichu;
            hanghoa.lamonthem = hangHoa.lamonthem;
            hanghoa.hinhthucban = hangHoa.hinhthucban;
            hanghoa.trangthai = hangHoa.trangthai;
            hanghoa.ThucDon = _context.ThucDons.Find(hangHoa.thucdonID);
            hanghoa.Loaihang = _context.Loaihangs.Find(hangHoa.loaihangID);
            hanghoa.NhomHang = _context.NhomHangs.Find(hangHoa.nhomhangID);
            hanghoa.mota = hangHoa.mota;
            _context.HangHoas.Add(hanghoa);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetHangHoa", new { id = hanghoa.hanghoaID }, hanghoa);
        }

        // DELETE: api/HangHoas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteHangHoa([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var hangHoa = await _context.HangHoas.FindAsync(id);
            if (hangHoa == null)
            {
                return NotFound();
            }

            _context.HangHoas.Remove(hangHoa);
            await _context.SaveChangesAsync();

            return Ok(hangHoa);
        }

        private bool HangHoaExists(int id)
        {
            return _context.HangHoas.Any(e => e.hanghoaID == id);
        }


        
        [HttpPost("getthongketonkho")]
        public async Task<IActionResult> Thongketonkho( DateRange daterange)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            DateTime thoigiantondau = new DateTime(daterange.startdate.Year, daterange.startdate.Month, daterange.startdate.Day-1);
            DateTime thoigiantoncuoi = new DateTime(daterange.enddate.Year, daterange.enddate.Month, daterange.enddate.Day+1);
           
            var listhangnhapbyphieunhap = _context.ChiTietPhieuNhaps.Where(
                    x => x.PhieuNhap.thoigian >= daterange.startdate && x.PhieuNhap.thoigian <= daterange.enddate
                    ).Select(x=>x.HangHoa).ToList();
            var listhangbanrabyhoadon = _context.ChiTietHoaDons.Where(x => x.HoaDon.ngaylap >= daterange.startdate && x.HoaDon.ngaylap <= daterange.enddate).Select(x => x.HangHoa).ToList();

            var lsthanghoa = listhangbanrabyhoadon.Union(listhangnhapbyphieunhap);
            List<TonKho> lsttonkho = new List<TonKho>();
            foreach (var item in lsthanghoa)
            {
                var tonkho = new TonKho();
                int tondau = (int)_context.ChiTietPhieuNhaps.Where(x => x.PhieuNhap.thoigian <= thoigiantondau).Select(x => x.HangHoa).Sum(x => x.tonkho) - _context.ChiTietHoaDons.Where(x => x.HoaDon.ngaylap <= thoigiantondau).Select(x => x.HangHoa).Sum(x => x.tonkho);
                int nhap = (int)_context.ChiTietPhieuNhaps.Where(
                    x => x.PhieuNhap.thoigian >= daterange.startdate && x.PhieuNhap.thoigian <= daterange.enddate
                    ).Sum(x=>x.soluong);
                int xuat = (int)_context.ChiTietHoaDons.Where(x => x.HoaDon.ngaylap >= daterange.startdate && x.HoaDon.ngaylap <= daterange.enddate).Sum(x=>x.soluong);
                int toncuoi = tondau + nhap - xuat;

                tonkho.mahang = item.mahanghoa;
                tonkho.tenhang = item.tenhang;
                tonkho.tondau = tondau;
                tonkho.nhap = nhap;
                tonkho.xuat = xuat;
                tonkho.toncuoi = toncuoi;
                lsttonkho.Add(tonkho);
            }
            return Ok(lsttonkho);
        }

        [HttpPost("getthongkenhaphang")]
        public async Task<IActionResult> Thongkenhaphang(DateRange daterange)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DateTime thoigiantondau = new DateTime(daterange.startdate.Year, daterange.startdate.Month, daterange.startdate.Day - 1);
            DateTime thoigiantoncuoi = new DateTime(daterange.enddate.Year, daterange.enddate.Month, daterange.enddate.Day + 1);

            var listhangnhapbyphieunhap = _context.ChiTietPhieuNhaps.Where(
                    x => x.PhieuNhap.thoigian >= daterange.startdate && x.PhieuNhap.thoigian <= daterange.enddate
                    ).Select(x => x.HangHoa).ToList();
            
            List<HangNhap> lsthangnhap = new List<HangNhap>();
            foreach (var item in listhangnhapbyphieunhap)
            {
                var hangnhap = new HangNhap();
                int tondau = (int)_context.ChiTietPhieuNhaps.Where(x => x.PhieuNhap.thoigian <= thoigiantondau).Select(x => x.HangHoa).Sum(x => x.tonkho);
                int soluong = (int)_context.ChiTietPhieuNhaps.Where(
                    x => x.PhieuNhap.thoigian > daterange.startdate && x.PhieuNhap.thoigian < daterange.enddate
                    ).Sum(x => x.soluong);
                int giatri = (int)_context.ChiTietPhieuNhaps.Where(
                    x => x.PhieuNhap.thoigian > daterange.startdate && x.PhieuNhap.thoigian < daterange.enddate
                    ).Sum(x => x.soluong*x.dongia);
                int toncuoi = (int)_context.ChiTietPhieuNhaps.Where(x => x.PhieuNhap.thoigian >= thoigiantoncuoi).Select(x => x.HangHoa).Sum(x => x.tonkho);


                hangnhap.mahang = item.mahanghoa;
                hangnhap.tenhang = item.tenhang;
                hangnhap.tondau = tondau;
                hangnhap.soluong = soluong;
                hangnhap.giatri = giatri;
                hangnhap.toncuoi = toncuoi;
                lsthangnhap.Add(hangnhap);
            }
            return Ok(lsthangnhap);
        }
    }
}