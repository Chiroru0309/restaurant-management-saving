﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models;
using RestaurantAPI.Models.Response;

namespace RestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhieuNhapsController : ControllerBase
    {
        private readonly DataContext _context;

        public PhieuNhapsController(DataContext context)
        {
            _context = context;
        }

        // GET: api/NhomHangs
        [HttpGet]
        public IEnumerable<PhieuNhap> GetPhieuNhaps()
        {
            return _context.PhieuNhaps;
        }

        // GET: api/NhomHangs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPhieuNhap([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var phieuNhap = await _context.PhieuNhaps.FindAsync(id);

            if (phieuNhap == null)
            {
                return NotFound();
            }

            return Ok(phieuNhap);
        }

        // PUT: api/NhomHangs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPhieuNhap([FromRoute] int id, [FromBody] PhieuNhap phieuNhap)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != phieuNhap.id)
            {
                return BadRequest();
            }

            _context.Entry(phieuNhap).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PhieuNhapExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/NhomHangs
        [HttpPost]
        public async Task<IActionResult> PostPhieuNhap([FromBody] PhieuNhap phieuNhap)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.PhieuNhaps.Add(phieuNhap);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPhieuNhap", new { id = phieuNhap.id }, phieuNhap);
        }

        // DELETE: api/NhomHangs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePhieuNhap([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var phieuNhap = await _context.PhieuNhaps.FindAsync(id);
            if (phieuNhap == null)
            {
                return NotFound();
            }

            _context.PhieuNhaps.Remove(phieuNhap);
            await _context.SaveChangesAsync();

            return Ok(phieuNhap);
        }

        private bool PhieuNhapExists(int id)
        {
            return _context.PhieuNhaps.Any(e => e.id == id);
        }
    }
}