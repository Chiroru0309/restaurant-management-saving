﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models;
using RestaurantAPI.Models.Request;
using RestaurantAPI.Models.Response;

namespace RestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChiTietHoaDonsController : ControllerBase
    {
        private readonly DataContext _context;

        public ChiTietHoaDonsController(DataContext context)
        {
            _context = context;
        }

        // GET: api/ChiTietHoaDons
        [HttpGet]
        public IEnumerable<ChiTietHoaDon> GetChiTietHoaDon()
        {
            return _context.ChiTietHoaDon.Include(x => x.HangHoa).Include(x => x.HoaDon);
        }

        // GET: api/ChiTietHoaDons/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetChiTietHoaDon([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var chiTietHoaDon = await _context.ChiTietHoaDon.FindAsync(id);

            if (chiTietHoaDon == null)
            {
                return NotFound();
            }

            return Ok(chiTietHoaDon);
        }

        // GET: api/ChiTietHoaDons/5
        [HttpGet("getchitiettheoban/{id}")]
        public  IEnumerable<ChiTietHoaDon> GetChiTietTheoBan([FromRoute] int id)
        {
            var hoadon = _context.HoaDon.Where(x => x.Ban.banID == id).Where(x=>x.tinhtrang==false).FirstOrDefault();
            return _context.ChiTietHoaDon.Where(x=>x.HoaDon.hoadonID==hoadon.hoadonID).Include(x => x.HangHoa).Include(x => x.HoaDon).ToList();
        }


        // PUT: api/ChiTietHoaDons/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutChiTietHoaDon([FromRoute] int id, [FromBody] ChiTietHoaDon chiTietHoaDon)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != chiTietHoaDon.chitiethoadonID)
            {
                return BadRequest();
            }

            _context.Entry(chiTietHoaDon).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChiTietHoaDonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ChiTietHoaDons
        [HttpPost]
        public async Task<IActionResult> PostChiTietHoaDon([FromBody] ChiTietHoaDonReq chiTietHoaDonreq)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var chiTietHoaDon = new ChiTietHoaDon();
            
      
            chiTietHoaDon.HangHoa = _context.HangHoas.Find(chiTietHoaDonreq.hanghoaID);
            chiTietHoaDon.HoaDon = _context.HoaDon.Find(chiTietHoaDonreq.hoadonID);
            chiTietHoaDon.ghichu = chiTietHoaDonreq.ghichu;
            chiTietHoaDon.giamgia = chiTietHoaDonreq.giamgia;
            chiTietHoaDon.soluong = chiTietHoaDonreq.soluong;


            _context.ChiTietHoaDon.Add(chiTietHoaDon);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetChiTietHoaDon", new { id = chiTietHoaDon.chitiethoadonID }, chiTietHoaDon);
        }

        // DELETE: api/ChiTietHoaDons/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteChiTietHoaDon([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var chiTietHoaDon = await _context.ChiTietHoaDon.FindAsync(id);
            if (chiTietHoaDon == null)
            {
                return NotFound();
            }

            _context.ChiTietHoaDon.Remove(chiTietHoaDon);
            await _context.SaveChangesAsync();

            return Ok(chiTietHoaDon);
        }

        private bool ChiTietHoaDonExists(int id)
        {
            return _context.ChiTietHoaDon.Any(e => e.chitiethoadonID == id);
        }
    }
}