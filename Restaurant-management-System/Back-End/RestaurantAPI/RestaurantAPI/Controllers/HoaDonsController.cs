﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models;
using RestaurantAPI.Models.Request;
using RestaurantAPI.Models.Response;

namespace RestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowSpecificOrigin")]
    [ApiController]
    public class HoaDonsController : ControllerBase
    {
        private readonly DataContext _context;

        public HoaDonsController(DataContext context)
        {
            _context = context;
        }

        // GET: api/HoaDons
        [HttpGet]
        public IEnumerable<HoaDonReq> GetHoaDon()
        {
            return _context.HoaDon.Select(x => new HoaDonReq
            {
                banID = x.Ban.banID,
                giamgia = x.giamgia,
                hoadonID = x.hoadonID,
                ngaylap = x.ngaylap,
                PhuongThucThanhToanId = x.PhuongThucThanhToan.ptttId,
                tiendatra = x.tiendatra,
                tinhtrang = x.tinhtrang
            }).ToList();
        }

        // GET: api/HoaDons/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetHoaDon([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var hoaDon = await _context.HoaDon.FindAsync(id);

            if (hoaDon == null)
            {
                return NotFound();
            }

            return Ok(hoaDon);
        }

        // GET: api/HoaDons/5
        [HttpGet("getOrderFromTableId/{id}")]
        public int getOrderFromTableId([FromRoute] int id)
        {
            return _context.HoaDon.Where(x => x.Ban.banID == id).Where(x => x.tinhtrang == false).FirstOrDefault().hoadonID;
        }

        // GET: api/HoaDons/5
        [HttpGet("getStatusOrder/{id}")]
        public bool GetStatusOrder([FromRoute] int id)
        {
            if (_context.HoaDon.Where(x => x.Ban.banID == id).Where(x=>x.tinhtrang==false).FirstOrDefault() != null) {

            return _context.HoaDon.Where(x => x.Ban.banID == id).Where(x => x.tinhtrang == false).FirstOrDefault().tinhtrang;
            }
            else
            {
                return true;
            }
        }

        // PUT: api/HoaDons/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutHoaDon([FromRoute] int id, [FromBody] HoaDon hoaDon)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hoaDon.hoadonID)
            {
                return BadRequest();
            }

            _context.Entry(hoaDon).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HoaDonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/HoaDons
        [HttpPost]
        public async Task<IActionResult> PostHoaDon([FromBody] HoaDonReq hoadonreq)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var hoadon = new HoaDon();
            hoadon.giamgia = hoadonreq.giamgia;
            hoadon.ngaylap = hoadonreq.ngaylap;
            hoadon.tiendatra = hoadonreq.tiendatra;
            hoadon.tinhtrang = hoadonreq.tinhtrang;
            hoadon.PhuongThucThanhToan = _context.PhuongThucThanhToan.Find(hoadonreq.PhuongThucThanhToanId);
            hoadon.Ban = _context.Ban.Find(hoadonreq.banID);

            _context.HoaDon.Add(hoadon);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetHoaDon", new { id = hoadon.hoadonID }, hoadon);
        }

        // DELETE: api/HoaDons/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteHoaDon([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var hoaDon = await _context.HoaDon.FindAsync(id);
            if (hoaDon == null)
            {
                return NotFound();
            }

            _context.HoaDon.Remove(hoaDon);
            await _context.SaveChangesAsync();

            return Ok(hoaDon);
        }

        private bool HoaDonExists(int id)
        {
            return _context.HoaDon.Any(e => e.hoadonID == id);
        }
    }
}