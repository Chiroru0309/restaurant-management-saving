﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models;
using RestaurantAPI.Models.Request;
using RestaurantAPI.Models.Response;

namespace RestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DVTsController : ControllerBase
    {
        private readonly DataContext _context;

        public DVTsController(DataContext context)
        {
            _context = context;
        }

        // GET: api/DVTs/getdvttheohanghoa/5
        [HttpGet("getdvttheohanghoa/{hanghoaid}")]
        public async Task<IActionResult> GetDVTTheoHangHoa([FromRoute] int hanghoaid)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dVT = await _context.DVT.Where(x=>x.Hanghoa.hanghoaID == hanghoaid).Select(x=> new {
                dvtid = x.dvtID,
                tendvt = x.ten,
                tenhanghoa = x.Hanghoa.tenhang,
                giatriquydoi = x.giatriquydoi,
                giaban = x.giaban,
                donviconban = x.donvicoban
            }).ToListAsync();

            if (dVT == null)
            {
                return NotFound();
            }

            return Ok(dVT);
        }

       
        // POST: api/DVTs
        [HttpPost]
        public async Task<IActionResult> PostDVT([FromBody] DVTReq dVT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var dvt = new DVT();
            dvt.dvtID = dVT.dvtid;
            dvt.ten = dVT.tendvt;
            dvt.Hanghoa.hanghoaID = dVT.hanghoaid;
            dvt.giaban = dvt.giaban;
            dvt.giatriquydoi = dVT.giatriquydoi;
            dvt.donvicoban = dVT.donvicoban;
            _context.DVT.Add(dvt);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDVT", new { id = dvt.dvtID }, dvt);
        }

        // DELETE: api/DVTs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDVT([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dVT = await _context.DVT.FindAsync(id);
            if (dVT == null)
            {
                return NotFound();
            }

            _context.DVT.Remove(dVT);
            await _context.SaveChangesAsync();

            return Ok(dVT);
        }

        private bool DVTExists(int id)
        {
            return _context.DVT.Any(e => e.dvtID == id);
        }
    }
}