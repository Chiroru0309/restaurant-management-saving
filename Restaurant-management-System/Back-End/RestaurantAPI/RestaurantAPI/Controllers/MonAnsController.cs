﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models;
using RestaurantAPI.Models.Request;
using RestaurantAPI.Models.Response;

namespace RestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MonAnsController : ControllerBase
    {
        private readonly DataContext _context;

        public MonAnsController(DataContext context)
        {
            _context = context;
        }

        
        // GET: api/MonAns/5
        [HttpGet("bynguyenlieu/{id}")]
        public IEnumerable<MonAnRes> GetMonCheBienByNguyenLieu([FromRoute] int id)
        {
            var monchebiens = _context.DBMonAns.Where(x => x.NguyenLieu.hanghoaID == id).Select(x => new MonAnRes()
            {
                monchebienID = x.id,
                tenmonan = x.MonCheBien.tenhang,
                tennguyenlieu = x.NguyenLieu.tenhang,
                giabanmonan = x.MonCheBien.giaban,
                giabannguyenlieu = x.NguyenLieu.giaban,
                soluong = x.soluong
            }).ToList();
            return monchebiens;
        }
        
        // GET: api/MonAns/5
        [HttpGet("bymonchebien/{id}")]
        public IEnumerable<MonAnRes> GetNguyenLieuByMonCheBien([FromRoute] int id)
        {
            var monchebiens = _context.DBMonAns.Where(x => x.MonCheBien.hanghoaID == id).Select(x => new MonAnRes()
            {
                monchebienID = x.id,
                tenmonan = x.MonCheBien.tenhang,
                tennguyenlieu = x.NguyenLieu.tenhang,
                giabanmonan = x.MonCheBien.giaban,
                giabannguyenlieu = x.NguyenLieu.giaban,
                soluong = x.soluong
            }).ToList();
            return monchebiens;
        }

        // POST: api/MonAns
        [HttpPost]
        public IActionResult PostMonThem([FromBody] MonAnReq monanreq)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var monan = new MonAn();
            monan.id = monanreq.moncehbienID;
            monan.MonCheBien = _context.HangHoas.Find(monanreq.monanID);
            monan.NguyenLieu = _context.HangHoas.Find(monanreq.nguyenlieuID);
            monan.soluong = monanreq.soluong;
            _context.DBMonAns.Add(monan);
            _context.SaveChangesAsync();

            return CreatedAtAction("GetMonThem", new { id = monan.id }, monan);
        }

        // DELETE: api/MonAns/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMonAn([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var monAn = await _context.DBMonAns.FindAsync(id);
            if (monAn == null)
            {
                return NotFound();
            }

            _context.DBMonAns.Remove(monAn);
            await _context.SaveChangesAsync();

            return Ok(monAn);
        }

        private bool MonAnExists(int id)
        {
            return _context.DBMonAns.Any(e => e.id == id);
        }
    }
}