﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models;
using RestaurantAPI.Models.Response;

namespace RestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NhomHangsController : ControllerBase
    {
        private readonly DataContext _context;

        public NhomHangsController(DataContext context)
        {
            _context = context;
        }

        // GET: api/NhomHangs
        [HttpGet]
        public IEnumerable<NhomHang> GetDBNhomHangs()
        {
            return _context.DBNhomHangs;
        }
        

        // GET: api/NhomHangs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetNhomHang([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var nhomHang = await _context.DBNhomHangs.FindAsync(id);

            if (nhomHang == null)
            {
                return NotFound();
            }

            return Ok(nhomHang);
        }

        // PUT: api/NhomHangs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutNhomHang([FromRoute] int id, [FromBody] NhomHang nhomHang)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != nhomHang.nhomhangID)
            {
                return BadRequest();
            }

            _context.Entry(nhomHang).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NhomHangExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/NhomHangs
        [HttpPost]
        public async Task<IActionResult> PostNhomHang([FromBody] NhomHang nhomHang)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.DBNhomHangs.Add(nhomHang);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetNhomHang", new { id = nhomHang.nhomhangID }, nhomHang);
        }

        // DELETE: api/NhomHangs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteNhomHang([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var nhomHang = await _context.DBNhomHangs.FindAsync(id);
            if (nhomHang == null)
            {
                return NotFound();
            }

            _context.DBNhomHangs.Remove(nhomHang);
            await _context.SaveChangesAsync();

            return Ok(nhomHang);
        }

        private bool NhomHangExists(int id)
        {
            return _context.DBNhomHangs.Any(e => e.nhomhangID == id);
        }
    }
}