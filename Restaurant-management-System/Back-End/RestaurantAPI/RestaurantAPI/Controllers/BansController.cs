﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models;
using RestaurantAPI.Models.Response;

namespace RestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowSpecificOrigin")]
    [ApiController]
    public class BansController : ControllerBase
    {
        private readonly DataContext _context;

        public BansController(DataContext context)
        {
            _context = context;
        }

        // GET: api/Bans
        [HttpGet]
        public IEnumerable<Ban> GetBan()
        {
            return _context.Ban.Where(x => x.nhombanID != 0).ToList();
        }
        // GET: api/Bans
        [HttpGet("getnhombans")]
        public IEnumerable<Ban> GetNhomBanBan()
        {
            return _context.Ban.Where(x => x.nhombanID == 0).ToList();
        }
        // GET: api/Bans
        [HttpGet("getbantheonhom/{id}")]
        public IEnumerable<Ban> GetBanTheoNhom([FromRoute] int id)
        {
            return _context.Ban.Where(x => x.nhombanID == id).ToList();
        }
        // GET: api/Bans/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBan([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ban = await _context.Ban.FindAsync(id);

            if (ban == null)
            {
                return NotFound();
            }

            return Ok(ban);
        }

        // PUT: api/Bans/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBan([FromRoute] int id, [FromBody] Ban ban)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ban.banID)
            {
                return BadRequest();
            }

            _context.Entry(ban).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BanExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Bans
        [HttpPost]
        public async Task<IActionResult> PostBan(Ban ban)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Ban.Add(ban);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBan", new { id = ban.banID }, ban);
        }

        // DELETE: api/Bans/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBan([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ban = await _context.Ban.FindAsync(id);
            if (ban == null)
            {
                return NotFound();
            }

            _context.Ban.Remove(ban);
            await _context.SaveChangesAsync();

            return Ok(ban);
        }

        private bool BanExists(int id)
        {
            return _context.Ban.Any(e => e.banID == id);
        }
    }
}