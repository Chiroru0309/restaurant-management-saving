﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models;
using RestaurantAPI.Models.Request;
using RestaurantAPI.Models.Response;

namespace RestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThuocTinhsController : ControllerBase
    {
        private readonly DataContext _context;

        public ThuocTinhsController(DataContext context)
        {
            _context = context;
        }


        // GET: api/ThuocTinhs/getthuoctinhtheohanghoa/5
        [HttpGet("getthuoctinhtheohanghoa/{id}")]
        public async Task<IActionResult> GetThuocTinhTheoHangHoa([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var thuocTinh = await _context.ThuocTinh.Where(x=>x.HangHoa.hanghoaID==id).Select(x=> new {
                thuoctinhid = x.thuoctinhID,
                tenthuoctinh = x.ten,
                tenhanghoa = x.HangHoa.tenhang,
                giatri = x.giatri
            }).ToListAsync();

            if (thuocTinh == null)
            {
                return NotFound();
            }

            return Ok(thuocTinh);
        }

        
        // POST: api/ThuocTinhs
        [HttpPost]
        public async Task<IActionResult> PostThuocTinh([FromBody] ThuocTinhReq thuocTinhreq)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var thuoctinh = new ThuocTinh();
            thuoctinh.thuoctinhID = thuocTinhreq.thuoctinhid;
            thuoctinh.ten = thuocTinhreq.tenthuoctinh;
            thuoctinh.HangHoa.hanghoaID = thuocTinhreq.hanghoaid;
            thuoctinh.giatri = thuocTinhreq.giatri;
            _context.ThuocTinh.Add(thuoctinh);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetThuocTinh", new { id = thuoctinh.thuoctinhID }, thuoctinh);
        }

        // DELETE: api/ThuocTinhs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteThuocTinh([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var thuocTinh = await _context.ThuocTinh.FindAsync(id);
            if (thuocTinh == null)
            {
                return NotFound();
            }

            _context.ThuocTinh.Remove(thuocTinh);
            await _context.SaveChangesAsync();

            return Ok(thuocTinh);
        }

        private bool ThuocTinhExists(int id)
        {
            return _context.ThuocTinh.Any(e => e.thuoctinhID == id);
        }
    }
}