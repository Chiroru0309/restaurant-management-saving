﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models;
using RestaurantAPI.Models.Response;
using RestaurantAPI.Models.Resquest;

namespace RestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowSpecificOrigin")]
    [ApiController]
    public class ChiTietPhieuNhapsController : ControllerBase
    {
        private readonly DataContext _context;

        public ChiTietPhieuNhapsController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<ChiTietPhieuNhap> GetAllHangHoaByPhieuNhap()
        {
            // The following line loads the contacts
            //return _context.ChiTietPhieuNhaps
            //    .Include(x=>x.PhieuNhap)
            //    .Include(x=>x.HangHoa)
            //    .Where(x=>x.PhieuNhap.id==id)
            //    .ToList();

            return _context.ChiTietPhieuNhaps
                .Include(x => x.PhieuNhap)
                .Include(x => x.HangHoa)
                .ToList();
        }


        // GET: api/PhieuNhaps
        [HttpGet("getAllHangHoaByPhieuNhap/{id}")]
        public IEnumerable<ChiTietPhieuNhap> GetAllHangHoaByPhieuNhap([FromRoute] string id)
        {
            // The following line loads the contacts
            //return _context.ChiTietPhieuNhaps
            //    .Include(x=>x.PhieuNhap)
            //    .Include(x=>x.HangHoa)
            //    .Where(x=>x.PhieuNhap.id==id)
            //    .ToList();

            return _context.ChiTietPhieuNhaps
                .Include(x => x.PhieuNhap)
                .Include(x => x.HangHoa)
                .Where(x => x.HangHoa.mahanghoa == id)
                .ToList();
        }


        // GET: api/PhieuNhaps/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPhieuNhap([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var phieuNhap = await _context.ChiTietPhieuNhaps.FindAsync(id);

            if (phieuNhap == null)
            {
                return NotFound();
            }

            return Ok(phieuNhap);

        }

        //PUT: api/PhieuNhaps/5
        [HttpPut("{chitietphieunhapID}")]
        public async Task<IActionResult> PutPhieuNhap([FromRoute] int chitietphieunhapID, [FromBody] PhieuNhapReq phieuNhap)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //if (chitietphieunhapID != phieuNhap.chitietphieunhapID)
            //{
            //    return BadRequest();
            //}
            var phieunhap = _context.ChiTietPhieuNhaps.Where(x => x.chitietphieunhapID == chitietphieunhapID).FirstOrDefault();
            if (phieuNhap == null)
            {
                return NotFound();
            }

            phieunhap.soluong = phieuNhap.soluong;

            _context.Entry(phieunhap).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PhieuNhapExists(chitietphieunhapID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetPhieuNhap", new { chitietphieunhapID = chitietphieunhapID }, phieuNhap);
        }

        // POST: api/PhieuNhaps
        [HttpPost("{mahanghoa}/{maphieunhap}")]
        public async Task<IActionResult> PostPhieuNhap([FromRoute] string mahanghoa, [FromRoute]string maphieunhap)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Random rnd = new Random();
            var hanghoa = _context.HangHoas.Where(x => x.mahanghoa == mahanghoa).FirstOrDefault();
            var phieunhap = _context.PhieuNhaps.Where(x => x.maphieunhap == maphieunhap).FirstOrDefault();
            ChiTietPhieuNhap chitietphieunhap = new ChiTietPhieuNhap();
            //chitietphieunhap.chitietphieunhapID = rnd.Next(20, 1000);
            chitietphieunhap.soluong = 1;
            chitietphieunhap.dongia = hanghoa.giaban;
            chitietphieunhap.giamgia = phieunhap.giamgia;
            chitietphieunhap.HangHoa = hanghoa;
            chitietphieunhap.PhieuNhap = phieunhap;
            //chitietphieunhap.PhieuNhap = new PhieuNhap();
            //chitietphieunhap.PhieuNhap.id = rnd.Next(1005, 2000);
            //chitietphieunhap.PhieuNhap.maphieunhap = "PN00002" + chitietphieunhap.PhieuNhap.ToString();
            //chitietphieunhap.PhieuNhap.giamgia = 0;
            //chitietphieunhap.PhieuNhap.nhacungcap = "Thach Cao";
            //chitietphieunhap.PhieuNhap.thoigian = DateTime.Now;
            //chitietphieunhap.PhieuNhap.trangthai = true;
            //chitietphieunhap.HangHoa = hanghoa;
            _context.ChiTietPhieuNhaps.Add(chitietphieunhap);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPhieuNhap", new { chitietphieunhapID = chitietphieunhap.chitietphieunhapID }, chitietphieunhap);
        }

        // DELETE: api/PhieuNhaps/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePhieuNhap([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var phieuNhap = await _context.ChiTietPhieuNhaps.FindAsync(id);
            if (phieuNhap == null)
            {
                return NotFound();
            }

            _context.ChiTietPhieuNhaps.Remove(phieuNhap);
            await _context.SaveChangesAsync();

            return Ok(phieuNhap);
        }

        private bool PhieuNhapExists(int chitietphieunhapID)
        {
            return _context.ChiTietPhieuNhaps.Any(e => e.chitietphieunhapID == chitietphieunhapID);
        }
    }
}