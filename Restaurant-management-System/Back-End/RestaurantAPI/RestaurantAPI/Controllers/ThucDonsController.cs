﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Models;
using RestaurantAPI.Models.Response;

namespace RestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThucDonsController : ControllerBase
    {
        private readonly DataContext _context;

        public ThucDonsController(DataContext context)
        {
            _context = context;
        }

        // GET: api/ThucDons
        [HttpGet]
        public IEnumerable<ThucDon> GetThucDons()
        {
            return _context.ThucDons;
        }

        // GET: api/ThucDons/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetThucDon([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var thucDon = await _context.ThucDons.FindAsync(id);

            if (thucDon == null)
            {
                return NotFound();
            }

            return Ok(thucDon);
        }

        // PUT: api/ThucDons/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutThucDon([FromRoute] int id, [FromBody] ThucDon thucDon)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != thucDon.thucdonID)
            {
                return BadRequest();
            }

            _context.Entry(thucDon).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ThucDonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ThucDons
        [HttpPost]
        public async Task<IActionResult> PostThucDon([FromBody] ThucDon thucDon)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ThucDons.Add(thucDon);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetThucDon", new { id = thucDon.thucdonID }, thucDon);
        }

        // DELETE: api/ThucDons/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteThucDon([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var thucDon = await _context.ThucDons.FindAsync(id);
            if (thucDon == null)
            {
                return NotFound();
            }

            _context.ThucDons.Remove(thucDon);
            await _context.SaveChangesAsync();

            return Ok(thucDon);
        }

        private bool ThucDonExists(int id)
        {
            return _context.ThucDons.Any(e => e.thucdonID == id);
        }
    }
}